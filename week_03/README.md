## Содержание
- [Содержание](#содержание)
- [Введение](#введение)
- [Топология инфраструктуры Microsoft](#топология-инфраструктуры-microsoft)
- [Лабораторная работа №6. Настройка сайтов и доступ по FTP](#лабораторная-работа-6-настройка-сайтов-и-доступ-по-ftp)
- [Лабораторная работа №7. Центры сертификации в Windows Server](#лабораторная-работа-7-центры-сертификации-в-windows-server)
- [Лабораторная работа №8. Настройка RDS](#лабораторная-работа-8-настройка-rds)
- [Материалы для выполнения лабораторных работ 6-8](#материалы-для-выполнения-лабораторных-работ-6-8)
		- [Лабораторная 6](#лабораторная-6)
		- [Лабораторная 7](#лабораторная-7)
		- [Лабораторная 8](#лабораторная-8)
	- [Видеоматериалы](#видеоматериалы)
		- [Общий материал](#общий-материал)
		- [Основы Windows Server](#основы-windows-server)
			- [TrainIT Hard](#trainit-hard)
			- [Зарубежный материал](#зарубежный-материал)
- [Отчет по выполнению задания](#отчет-по-выполнению-задания)
		- [Лабораторная работа №6](#лабораторная-работа-6)
		- [Лабораторная работа №7](#лабораторная-работа-7)
		- [Лабораторная работа №8](#лабораторная-работа-8)

## Введение
Ниже представлена топология инфраструктуры, которая получится у вас по итогу прохождения курса. Лабораторные работы 6-8 представлены ниже. В самом конце присутствуют материалы, которые помогут выполнить данные лабораторные работы. Перед выполнением настоятельно рекомендуется изучить указанные материалы от TrainIT Hard.

Все нижеописанные лабораторные работы прорешаны, но могут возникать неточности. В случае их нахождения, а также нахождения каких-либо багов просьба подсвечивать.

Файл с заданием можно посмотреть и скачать отдельно, также как и файл с материалами и с проверкой.

Отчет присылать в формате Word файла на электронную почту.

Ссылки на образы с Яндекс диска:
- [Windows Server 2019](https://disk.yandex.ru/d/44SbQmvP3eS8HA)
- [Windows 10 1809 LTSC](https://disk.yandex.ru/d/Ug0FF9a5hC7aJA)

---
## Топология инфраструктуры Microsoft
![Топология инфраструктуры](https://gitlab.com/ivan_storm/git_windows_kurs/raw/main/windows_scheme_2.0.png)

## Лабораторная работа №6. Настройка сайтов и доступ по FTP
1. Установить на сервер __WIN-SRV-LAB03__ роль IIS
2. Создать веб-сайт и настроить к нему доступ:
   1. Имя сайта - __grensite.grenlab.local__
   2. Создать DNS-запись __grensite__ на сервере в зоне __grenlab.local__
   3. В качестве каталога для сайта использовать __C:\grenlabsite__
   4. В каталог с сайтом разместить следующие архив __[site_01.zip]__](https://gitlab.com/ivan_storm/git_windows_kurs/-/blob/8e3ebcd556b1d263a3a53e78b1a477d6d74dcd3e/week_03/site_01.zip)
   5. Доступ к сайту должен быть разрешен только из внутренней подсети основного офиса и только по доменному имени
3. Установить и настроить __FTP-сервер__:
   1. В качестве каталога использовать папку __C:\ftp__
   2. В каталог распаковать следующий архив __ftp.zip__
   3. Сделать доступ на чтение и редактирование локальной группе __ftp_users__. В эту группу должны входить доменные пользователи __NetworkDevice-MGMT-Admins__
   4. Ресурс должен быть доступен по имени __ftp.grenlab.local__
   5. __Анонимный доступ__ должен быть отключен
   6. Межсетевой экран Windows должен быть настроен на работу с FTP
   7. Подключение должно быть разрешено только из __серверной подсети__ и с хоста __WIN-WD-LAB02__
   8. С хоста __WIN-WD-LAB02__ загрузить какой-нибудь файл на FTP-сервер
4. Создать второй сайт для размещения в нем будущих сертификатов и CRL
   1. Создать каталог __C:\grencerts__
   2. Настроить возможность просматривать файлы в каталоге, положить туда какие-нибудь файлы
   3. Сайт должен быть доступен по доменному имени __certs.grenlab.local__ только по протоколу __http__
   4. Включить поддержку __DoubleEscaping__
   
## Лабораторная работа №7. Центры сертификации в Windows Server
1. Установить роль __AD Certification Authority__ на сервер __WIN-SRV-LAB01__
   1. Имя центра сертификации - __Grenlab-RootCA__
   2. Срок действия сертификата - __3 года__
   3. Сертификат подчиненого CA должен действовать __1 год__
2. Установить __SubCA__ на сервер __WIN-SRV-LAB03__
   1. Имя подчиненного центра сертификации - __Grenlab-SubCA__
   2. В выпускаемых сертификатах должны присутсовать ссылки на списки отзыва сертификатов (CRL, Delta CRL), также должна присутствовать ссылка на корневой сертификат
   3. Списки отзыва должны быть размещены по пути http://certs.grenlab.local/crl, а корневой сертификат - http://certs.grenlab.local/aia
   4. Сертификаты должны без проблем скачиваться на хосты (помним про спецсимволы в файлах)
3. Создать шаблоны сертификатов для следующих ресурсов:
   1. Веб-сайты
      - Имя шаблона - Web-Sites
      - Subject Name - Supply the request
      - Security - Domain Admins, Authenticated Users (Enroll)
      - Validaty Period - 1 год
      - Разрешить экспортировать приватный ключ
   2. Клиентские машины
      - Имя шаблона - WD-Clients
      - Validaty Period - 6 месяцев
      - Subject Name - брать информацию из Active Directory, формат имени - Common name, также включать информацию - DNS и UPN
      - Security - Domain Computers, autoenrollment разрешен
4. Выпустить следующие сертификаты:
   1. Сайт __grensite.grenlab.local__
      - Common Name - grensite.grenlab.local
      - DNS - grensite.grenlab.local, grensite
   2. Портал __rds.grenlab.local__
      - Common Name - rds.grenlab.local
      - DNS - rds.grenlab.local, rds
   3. Сделать GPO для автоматического выпуска сертификата компьютерам (серт должен выпускаться автоматически)
5. Привязать к сайту ранее выпущенный сертификат, сделать редирект с __http__ на __https__ через модуль __[URL rewrite](https://iis-umbraco.azurewebsites.net/downloads/microsoft/url-rewrite)__
6. Проверить доступность сайта __grensite.grenlab.local__ по __https__, сертификат должен быть доверенным. В Firefox отдельно добавить корневой сертификат в доверенные
7. Отозвать выпущенный для сайта сертификат
   1. Причину отзыва выбрать 
   2. Убедиться, что Internet Explorer выдает сообщение о том, что сертификат отозван
7. Проверить работу __FTP__ 
8. Проверить, что корневой сертификат появился на доменных ПК и серверах

## Лабораторная работа №8. Настройка RDS
1. Установить на сервер __WIN-SRV-LAB03__ роль __RDS__ (сервер лицензий не ставить)
   1. Использовать сценарий развертывания __Session-based desktop deployment__
   2. Все компоненты должны находиться на данном сервере
   3. Настроить доступ к __RDS-Web__ по доменному имени __rds.grenlab.local__
   4. Настроить перенаправление с __http__ на __https__ (через http редирект, либо через url rewrite)
2. Создать коллекцию с именем __RDS-Applications__
   1. Доступ к коллекции __RDS-Applications__ должен быть для всех __доменных пользователей__
   2. Создать две группы безопасности в глобальном OU __grenlab__ - __RDS-Putty-Users__ и __RDS-Paint-Users__, добавить в группу __RDS-Putty-Users__ всех сетевых администраторов, а также любого пользователя на свое усмотрение. В группу __RDS-Paint-Users__ добавить 4-х пользователей на свое усмотрение. 
   3. В коллекции опубликовать приложение __WordPad__, сделать его доступным всем пользователям коллекции.
   4. Опубликовать __Paint__ для пользователей групы __RDS-Paint-Users__, остальным пользователям приложение отображаться не должно
   5. Установить на сервер программу __Putty__ и опубликовать её группе __RDS-Putty-Users__
3. Настроить __SSO__ только для запуска приложений RDS (применить GPO, имя политики __RDS_SSO__)
   1. Вход на сам веб-портал должен быть с указанием УЗ
   2. Запуск приложений через __Internet Explorer__ должен происходить при клике мышкой на __иконку приложения__
   3. Не должно всплывать никаких предупреждений и окон безопасности при запуске RDP файла
4. Создать групповую политику для публикации всем доменным пользователям __ярлыка на портал RDS__
   1. Название политики - __RDSWebShortcut__
   2. Название ярлыка - __RDS Web Portal__
   3. Ярлык должен открывать портал RDS через __Internet Explorer__
5. Проверить наличие ярлыка, доступ к порталу __RDS Web__ и работоспособность опубликованных приложений под пользователями
---
## Материалы для выполнения лабораторных работ 6-8
### Текстовый материал
#### Лабораторная 6
- [Создание сайта IIS в Windows Server](https://pyatilistnik.org/creating-an-iis-site-in-windows-server-2012-r2/)
- [Настройка FTP сервера в Windows Server](https://winitpro.ru/index.php/2014/10/31/ftp-sajt-s-izolyaciej-polzovatelej-na-windows-server-2012-r2/)
- [Установка и настройка WebDAV в Windows Server](https://winitpro.ru/index.php/2011/12/13/ustanovka-i-nastrojka-webdav-na-iis-v-windows/)
- [Создание WebDAV-сервера в Windows](https://helpcenter.onlyoffice.com/ru/installation/groups-connect-webdav-windows.aspx)
---
#### Лабораторная 7
- [Установка центра сертификации на предприятии. Часть 1](https://habr.com/ru/companies/microsoft/articles/348944/)
- [Установка центра сертификации на предприятии. Часть 2](https://habr.com/ru/companies/microsoft/articles/348956/)
- [Установка центра сертификации на предприятии. Часть 3](https://habr.com/ru/companies/microsoft/articles/349202/)
- [Установка и настройка Active Directory Certificate Services](https://abuzov.com/active-directory-certificate-services/)
- [Установка подчиненного центра сертификации Microsoft CA](https://trinosoft.com/index.php?page=/is/prpzs.php)
- [Настройка расширений CDP и AIA в CA](https://learn.microsoft.com/ru-ru/windows-server/networking/core-network-guide/cncg/server-certs/configure-the-cdp-and-aia-extensions-on-ca1?source=recommendations)
---
#### Лабораторная 8
- [Что такое терминальная ферма RDS?](https://pyatilistnik.org/what-is-rds-terminal-farm/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 1 — РАЗВЁРТЫВАНИЕ В ДОМЕНЕ](https://beardedsysadmin.wordpress.com/2014/01/20/deployment-rds-within-domain/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 2 — СОЗДАНИЕ И НАСТРОЙКА КОЛЛЕКЦИЙ СЕАНСОВ](https://beardedsysadmin.wordpress.com/2014/01/24/rds-collections/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 3 — ПУБЛИКАЦИЯ И НАСТРОЙКА УДАЛЁННЫХ ПРИЛОЖЕНИЙ REMOTEAPP](https://beardedsysadmin.wordpress.com/2014/01/30/rds-remoteapp/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 4 — РАСПРОСТРАНЕНИЕ ПРИЛОЖЕНИЙ REMOTEAPP И УДАЛЁННЫХ РАБОЧИХ СТОЛОВ](https://beardedsysadmin.wordpress.com/2014/02/10/rds-delivery-remoteapps/)
- [Установка и настройка фермы Remote Desktop Services (RDS)](https://winitpro.ru/index.php/2022/02/17/ustanovka-nastrojka-remote-desktop-services-rds-windows-server/)
- [RDS: прозрачная аутентификация SSO (Single Sign-On) в Windows Server](https://winitpro.ru/index.php/2015/06/19/prozrachnaya-avtorizaciya-na-rds-s-pomoshhyu-sso-single-sign-on/)
- [Enabling SSO](https://woshub.com/sso-single-sign-on-authentication-on-rds/)
- [Как работает SSO](https://habr.com/ru/companies/nixys/articles/563244/)
- [Публикация приложений в RDS](https://adminotes.ru/publikatsiya-i-nastrojka-udalennyh-prilozhenij-remoteapp/)
---
### Видеоматериалы

#### Основы Windows Server
##### TrainIT Hard
- [Урок 11, часть 1 - Certification Authority, теория и установка)](https://www.youtube.com/watch?v=WK9FSaXQARU&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=11)
- [Урок 11, часть 2 - Web Server, CA - CDP, AIA](https://www.youtube.com/watch?v=bpzFfODvQms&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=12)
- [Урок 11, часть 3 - CA, выпуск сертификатов](https://www.youtube.com/watch?v=j84i-ppJhAM&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=13)
- [Урок 13 - Remote Desktop Services (Terminal Server)](https://www.youtube.com/watch?v=V1_JZOqsgAU&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=16)
##### Зарубежный материал


## Отчет по выполнению задания

#### Лабораторная работа №6
Результаты выполнения команд в виде скриншотов.
1. Результаты выполнения команд в __Powershell__ с машин __WIN-SRV-LAB01__ и __WIN-WD-LAB01__:
   - hostname
   - get-timezone
   - w32tm /query /source
   - ping win-srv-lab01, ping win-wd-lab01 (выполнять команды с обеих машин)
   - ipconfig /all
2. С машины __WIN-SRV-LAB01__ прислать следующие скриншоты:
   - Содержимое локальных групп и пользователей
   - Содержимое группы Administrators
   - Права на общую папку MyShare
   - Рабочий стол с наличием браузера MS Edge
3. С машины __WIN-WD-LAB01__ прислать скриншоты:
   - Экран блокировки своего пользователя и администратора
   - Картинка рабочего стола своего пользователя и администратора
   - Скриншоты содержимого файла из команды gpresult /h result.html
   - Запущенный скрипт матрицы при входе в систему, скриншот содержимого задачи из планировщика
   - telnet atomskills.tech 443
   - nslookup win-srv-lab01, nslookup it-learn.space
   - get-partition
   - net use под своим пользователем (у которого должны быть подключены диски), скриншот основного каталога __Этот компьютер__
   - Содержимое архива с логами
   - Код скрипта на установку налогоплательщика
  
#### Лабораторная работа №7
Результаты выполнения команд в виде скриншотов.
1. Скриншот системы WIN-SRV-LAB02
2. На WIN-SRV-LAB02 выполнить команды:
    - hostname
    - get-timezone
    - ipconfig /all
    - nslookup it-learn.space
    - w32tm /query /source
    - Get-DhcpServerv4Scope
    - Get-DhcpServerv4ExclusionRange
    - Get-DhcpServerv4Reservation -ScopeID 10.0.30.0
    - Get-DhcpServerv4Lease -ScopeId 10.0.20.0 (также для 10.0.30.0)
    - Get-DhcpServerv4Failover
3. С машины WIN-SRV-LAB01 пингануть LAB02
4. Скриншот с сервера WIN-SRV-LAB01 из менеджера управления серверами, вкладка All Servers
5. На WIN-SRV-LAB01 выполнить следующие команды:
    - Get-DhcpServerv4Failover
    - Get-DnsServerZone
    - Get-DnsServerForwarder
    - Get-DnsServerResourceRecord -ZoneName skynet.local
6. С машины WIN-SRV-LAB01 сделать скриншоты:
    - Включенный алгоритм Round robin
	- Невозможность добавления записи с кириллицей
7. На машине WIN-WD-LAB01 выполнить команды:
    - ipconfig /all
    - nslookup yandex.ru
	- nslookup win-srv-lab01.skynet.local
	- nslookup win-srv-lab01
	- ping win-srv-lab01.skynet.local
	- ping win-srv-lab01
8. Прислать скриншот с WIN-WD-LAB01 открытых сайтов в интернете (например, vk.com)