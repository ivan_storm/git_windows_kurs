# Материалы для выполнения лабораторных работ 6-8
---
### Текстовый материал
#### Лабораторная 6
- [Создание сайта IIS в Windows Server](https://pyatilistnik.org/creating-an-iis-site-in-windows-server-2012-r2/)
- [Настройка FTP сервера в Windows Server](https://winitpro.ru/index.php/2014/10/31/ftp-sajt-s-izolyaciej-polzovatelej-na-windows-server-2012-r2/)
- [Установка и настройка WebDAV в Windows Server](https://winitpro.ru/index.php/2011/12/13/ustanovka-i-nastrojka-webdav-na-iis-v-windows/)
- [Создание WebDAV-сервера в Windows](https://helpcenter.onlyoffice.com/ru/installation/groups-connect-webdav-windows.aspx)
---
#### Лабораторная 7
- [Установка центра сертификации на предприятии. Часть 1](https://habr.com/ru/companies/microsoft/articles/348944/)
- [Установка центра сертификации на предприятии. Часть 2](https://habr.com/ru/companies/microsoft/articles/348956/)
- [Установка центра сертификации на предприятии. Часть 3](https://habr.com/ru/companies/microsoft/articles/349202/)
- [Установка и настройка Active Directory Certificate Services](https://abuzov.com/active-directory-certificate-services/)
- [Установка подчиненного центра сертификации Microsoft CA](https://trinosoft.com/index.php?page=/is/prpzs.php)
- [Настройка расширений CDP и AIA в CA](https://learn.microsoft.com/ru-ru/windows-server/networking/core-network-guide/cncg/server-certs/configure-the-cdp-and-aia-extensions-on-ca1?source=recommendations)

#### Лабораторная 8
- [Что такое терминальная ферма RDS?](https://pyatilistnik.org/what-is-rds-terminal-farm/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 1 — РАЗВЁРТЫВАНИЕ В ДОМЕНЕ](https://beardedsysadmin.wordpress.com/2014/01/20/deployment-rds-within-domain/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 2 — СОЗДАНИЕ И НАСТРОЙКА КОЛЛЕКЦИЙ СЕАНСОВ](https://beardedsysadmin.wordpress.com/2014/01/24/rds-collections/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 3 — ПУБЛИКАЦИЯ И НАСТРОЙКА УДАЛЁННЫХ ПРИЛОЖЕНИЙ REMOTEAPP](https://beardedsysadmin.wordpress.com/2014/01/30/rds-remoteapp/)
- [RDS НА ОСНОВЕ СЕАНСОВ В WINDOWS SERVER 2012 R2. ЧАСТЬ 4 — РАСПРОСТРАНЕНИЕ ПРИЛОЖЕНИЙ REMOTEAPP И УДАЛЁННЫХ РАБОЧИХ СТОЛОВ](https://beardedsysadmin.wordpress.com/2014/02/10/rds-delivery-remoteapps/)
- [Установка и настройка фермы Remote Desktop Services (RDS)](https://winitpro.ru/index.php/2022/02/17/ustanovka-nastrojka-remote-desktop-services-rds-windows-server/)
- [RDS: прозрачная аутентификация SSO (Single Sign-On) в Windows Server](https://winitpro.ru/index.php/2015/06/19/prozrachnaya-avtorizaciya-na-rds-s-pomoshhyu-sso-single-sign-on/)
- [Enabling SSO](https://woshub.com/sso-single-sign-on-authentication-on-rds/)
- [Как работает SSO](https://habr.com/ru/companies/nixys/articles/563244/)
- [Публикация приложений в RDS](https://adminotes.ru/publikatsiya-i-nastrojka-udalennyh-prilozhenij-remoteapp/)
---
### Видеоматериалы
#### Общий материал


#### Основы Windows Server
##### TrainIT Hard
- [Урок 11, часть 1 - Certification Authority, теория и установка)](https://www.youtube.com/watch?v=WK9FSaXQARU&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=11)
- [Урок 11, часть 2 - Web Server, CA - CDP, AIA](https://www.youtube.com/watch?v=bpzFfODvQms&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=12)
- [Урок 11, часть 3 - CA, выпуск сертификатов](https://www.youtube.com/watch?v=j84i-ppJhAM&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=13)
- [Урок 13 - Remote Desktop Services (Terminal Server)](https://www.youtube.com/watch?v=V1_JZOqsgAU&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=16)
##### Зарубежный материал

