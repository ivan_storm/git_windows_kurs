## Отчет по выполнению задания

### Лабораторная работа №1
1. Результаты выполнения команд в __Powershell__ с машин __WIN-SRV-LAB01__ и __WIN-WD-LAB01__:
   - hostname
   - get-timezone
   - w32tm /query /source
   - ping win-srv-lab01, ping win-wd-lab01 (выполнять команды с обеих машин)
   - ipconfig /all
2. С машины __WIN-SRV-LAB01__ прислать следующие скриншоты:
   - Содержимое локальных групп и пользователей
   - Содержимое группы Administrators
   - Права на общую папку MyShare
   - Рабочий стол с наличием браузера MS Edge
3. С машины __WIN-WD-LAB01__ прислать скриншоты:
   - Экран блокировки своего пользователя и администратора
   - Картинка рабочего стола своего пользователя и администратора
   - Скриншоты содержимого файла из команды gpresult /h result.html
   - Запущенный скрипт матрицы при входе в систему, скриншот содержимого задачи из планировщика
   - telnet atomskills.tech 443
   - nslookup win-srv-lab01, nslookup it-learn.space
   - get-partition
   - net use под своим пользователем (у которого должны быть подключены диски), скриншот основного каталога __Этот компьютер__
   - Содержимое архива с логами
   - Код скрипта на установку налогоплательщика
  
### Лабораторная работа №2
1. Скриншот системы WIN-SRV-LAB02
2. На WIN-SRV-LAB02 выполнить команды:
    - hostname
    - get-timezone
    - ipconfig /all
    - nslookup it-learn.space
    - w32tm /query /source
    - Get-DhcpServerv4Scope
    - Get-DhcpServerv4ExclusionRange
    - Get-DhcpServerv4Reservation -ScopeID 10.0.30.0
    - Get-DhcpServerv4Lease -ScopeId 10.0.20.0 (также для 10.0.30.0)
    - Get-DhcpServerv4Failover
3. С машины WIN-SRV-LAB01 пингануть LAB02
4. Скриншот с сервера WIN-SRV-LAB01 из менеджера управления серверами, вкладка All Servers
5. На WIN-SR-LAB01 выполнить следующие команды:
    - Get-DhcpServerv4Failover
    - Get-DnsServerZone
    - Get-DnsServerForwarder
    - Get-DnsServerResourceRecord -ZoneName skynet.local
6. С машины WIN-SRV-LAB01 сделать скриншоты:
    - Включенный алгоритм Round robin
    - Невозможность добавления записи с кириллицей
7. На машине WIN-WD-LAB01 выполнить команды:
    - ipconfig /all
    - nslookup yandex.ru
	 - nslookup win-srv-lab01.skynet.local
	 - nslookup win-srv-lab01
	 - ping win-srv-lab01.skynet.local
	 - ping win-srv-lab01
8. Прислать скриншот с WIN-WD-LAB01 открытых сайтов в интернете (например, vk.com)
