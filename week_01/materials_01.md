# Материалы для выполнения лабораторных работ 1-2
---
### Текстовый материал
#### Лабораторная 1
- [Установка и настройка Windows Server на примере версии 2022](https://pyatilistnik.org/installing-and-configuring-windows-server-2021/)
- [Установка и настройка Windows Server Core](https://pyatilistnik.org/install-and-configure-windows-server-2019-core/)
- [Файл hosts](https://windowsnotes.ru/other/tajna-fajla-hosts/)
- [DISM - управление компонентами образов Windows](https://ab57.ru/cmdlist/dism.html)
- [Использование nslookup](https://vmblog.ru/ispolzvanie-komandy-nslookup-v-windows/)
- [Обзор средства управления дисками](https://learn.microsoft.com/ru-ru/windows-server/storage/disk-management/overview-of-disk-management)
- [Локальные пользователи и группы](https://webistore.ru/administrirovaniye-windows/gruppy-polzovatelej-v-windows-lokalnye-polzovateli-i-gruppy/)
- [Управление локальными групповыми политиками](https://1cloud.ru/help/windows/upravlenie-lokalnymi-gruppovymi-politikami)
- [Права доступа к файлам и папкам простым языком](http://vasilisc.com/simple-permissions)
- [Планировщик заданий](https://pc.ru/docs/windows/task-scheduler)
- [Журнал событий](https://serverspace.ru/support/help/zhurnaly-windows/)
- [Общий доступ к папкам и файлам](https://winitpro.ru/index.php/2018/06/21/obshchij-dostup-k-setevym-papkam-i-printeram-bez-homegroup/)
- [Основы командной строки Windows](https://info-comp.ru/vseowindowsst/53-comandstroka.html)
- [Powershell для начинающих](https://tproger.ru/translations/powershell-tutorial)
- [Написание BAT-скриптов своими руками](https://selectel.ru/blog/bat-file/)

---
#### Лабораторная 2
- [Настройка DHCP failover](https://windowsnotes.ru/windows-server-2016/nastrojka-dhcp-failover-v-windows-server-2016/)
- [DHCP Relay в Windows Server](https://winitpro.ru/index.php/2013/08/22/nastrojka-dhcp-relay-agent-v-windows-server-2012/)
- [Server Manager и рабочие группы](https://windowsnotes.ru/windows-server-2012/windows-server-2012-nastrojka-server-manager-v-rabochix-gruppax/)
- [Настройка NAT](https://interface31.ru/tech_it/2010/02/windows-server-nastrojka-nat-dhcp.html)
- [Установка и настройка DHCP сервера](https://winitpro.ru/index.php/2015/11/10/nastrojka-dhcp-servera-s-pomoshhyu-powershell/)
- [Установка и настройка DNS сервера](https://serverspace.ru/support/help/configuring-a-dns-server-on-windows-server-2012-or-later/)
---
### Видеоматериалы
#### Общий материал
- [Виртуализация](https://www.youtube.com/watch?v=WWNfsRb5RSU&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=3&ab_channel=TrainITHard)
- [Сетевые сервисы](https://www.youtube.com/watch?v=IyuOLtZ8QM0&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=5&ab_channel=TrainITHard)
- [Разрешения файловой системы](https://www.youtube.com/watch?v=4b6ypDbGW3c&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=13&ab_channel=TrainITHard)
- [Разворачиваем тестовый стенд](https://www.youtube.com/watch?v=MtnukhCnK_4&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=11&ab_channel=TrainITHard)

#### Основы Windows Server
##### TrainIT Hard
 - [Урок 2 - Server Manager, настройка](https://www.youtube.com/watch?v=h0veujXjZV0&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=2&ab_channel=TrainITHard)
 - [Урок 3 - DHCP сервер](https://www.youtube.com/watch?v=ymUf8mUTSaY&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=3&ab_channel=TrainITHard)
 - [Урок 4 - Основы DNS](https://www.youtube.com/watch?v=dXkC3L9IdEk&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=4&ab_channel=TrainITHard)

##### Зарубежный материал
 - [Windows Server Core Installation](https://www.youtube.com/watch?v=hzZPy4RhL-0&list=PLUZTRmXEpBy32NP6z_qvVBOTWUzdTZVHt&index=9&ab_channel=MSFTWebCast)
 - [Basic Configuration tasks in Windows Server 2019](https://www.youtube.com/watch?v=1nxYJSV7-u8&list=PLUZTRmXEpBy32NP6z_qvVBOTWUzdTZVHt&index=2&ab_channel=MSFTWebCast)



    