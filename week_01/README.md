## Содержание
- [Содержание](#содержание)
- [Введение](#введение)
- [Топология инфраструктуры Microsoft](#топология-инфраструктуры-microsoft)
- [Лабораторная работа №1. Знакомство с Windows Server и углубленная работа с Windows 10](#лабораторная-работа-1-знакомство-с-windows-server-и-углубленная-работа-с-windows-10)
- [Лабораторная работа №2. Конфигурация сети в Windows Server (DHCP, DNS, маршрутизация и NAT)](#лабораторная-работа-2-конфигурация-сети-в-windows-server-dhcp-dns-маршрутизация-и-nat)
- [Материалы для выполнения лабораторных работ 1-2](#материалы-для-выполнения-лабораторных-работ-1-2)
		- [Лабораторная 1](#лабораторная-1)
		- [Лабораторная 2](#лабораторная-2)
	- [Видеоматериалы](#видеоматериалы)
		- [Общий материал](#общий-материал)
		- [Основы Windows Server](#основы-windows-server)
			- [TrainIT Hard](#trainit-hard)
			- [Зарубежный материал](#зарубежный-материал)
- [Отчет по выполнению задания](#отчет-по-выполнению-задания)
		- [Лабораторная работа №1](#лабораторная-работа-1)
		- [Лабораторная работа №2](#лабораторная-работа-2)

## Введение
Ниже представлена топология инфраструктуры, которая получится у вас по итогу прохождения курса. Лабораторные работы 1 и 2 представлены ниже. В самом конце присутствуют материалы, которые помогут выполнить данные лабораторные работы. Перед выполнением настоятельно рекомендуется изучить указанные материалы от TrainIT Hard.

Обе лабораторные полностью прорешаны в полном объеме. В случае нахождения каких-либо багов или неточностей в описании просьба подсвечивать.

Лабораторные работы полностью прорешаны в ProxmoxVE. Можно использовать Virtual Box, Vmware Workstation Pro, Hyper-V или Vmware EsXi. Что использовать выбирайте самостоятельно. Деление на подсети с помощью виртуальных адаптеров. Например, в VMware Workstation вы можете использовать встроенные Vmnet, в Proxmox и Esxi эти сети нужно создавать самостоятельно. В случае нехватки ресурсов рекомендуется создавать ВМ в VMware Workstation Pro или VirtualBox.
Если у вас 8 Гб ОЗУ или меньше, то можно убрать клиентскую машину WIN-WD-LAB02, сервер WIN-SRV-LAB03. На клиентской машине можно выдать 1 гб ОЗУ, на WIN-SRV-LAB02 - 1 гб ОЗУ, на основной сервер 3-4 Гб. 

Отчет присылать в формате одного Word файла на электронную почту.

Образы рекомендуется использовать по ссылкам ниже. В качестве клиентских ОС использовать только LTSC или Education.

Ссылки на образы с Яндекс диска:
- [Windows Server 2019](https://disk.yandex.ru/d/44SbQmvP3eS8HA)
- [Windows 10 1809 LTSC](https://disk.yandex.ru/d/Ug0FF9a5hC7aJA)

---
## Топология инфраструктуры Microsoft
![Топология инфраструктуры](https://gitlab.com/ivan_storm/git_windows_kurs/raw/main/windows_scheme_2.0.png)

## Лабораторная работа №1. Знакомство с Windows Server и углубленная работа с Windows 10
1. Создать две виртуальные машины, одну Windows Server 2019 (использовать версию __Standard с GUI__), другую Windows 10 LTSC. Рекомендованное дисковое пространство каждой ВМ по __30гб__ (если мало места на диске, то рекомендуется использовать тонкие (динамические) диски), также можно уменьшить объем диска.
2. На Windows Server 2019 выполнить следующие настройки:
	1. Задать имя сервера __WIN-SRV-LAB01__
  	2. Изменить часовой пояс на __Russian Standard Time__ (Москва +3)
  	3. Отключить защиту в __Internet Explorer__
  	4. Задать статический IP-адрес серверу, в качестве сервера DNS использовать Яндекс __77.88.8.8__
  	5. Разрешить серверу отвечать на ICMP (пинг)
  	6. Создать пользователя со своим именем (например, Ivan) с паролем __P@ssw0rd__, добавить пользователя в группу локальных администраторов
  	7. Установить браузер __Microsoft Edge__
  	8. В корне диска C создать каталог __MyShare__ и предоставить общий доступ своему пользователю. Права на __чтение и запись__.
  	9. Выполнить синхронизацию времени с __ntp0.ntp-servers.net__
3. На Windows 10 выполнить следующие настройки:
	1. Задать имя компьютера __WIN-WD-LAB01__
	2. Изменить часовой пояс на __Russia Standard Time__ (Москва +3)
	3. Включить встроенную учетную запись администратора (__Администратор__), задать пароль __P@ssw0rd__
	4. Создать пользователя со своим именем с паролем __P@ssw0rd__, добавить его в группу локальных администраторов и залогиниться под ним
	5. Задать статический ip-адрес из той же подсети, что и Windows-сервер. В качестве DNS использовать яндексовский
	6. В файле __hosts__ прописать DNS-запись для установленного Windows-сервера. Имя записи должно совпадать с именем Windows-сервера, также сделать запись на самого себя. Аналогичные записи сделать на __WIN-SRV-LAB01__
	7. Включить __Telnet-клиент__ в системе через командную строку с применением Powershell или утилиты DISM
	8. Проверить доступность Windows-сервера по имени через утилиту __ping__
	9. Проверить телнетом доступность веб-сайта __stepik.org__ по __443__ порту
	10. Выполнить синхронизацию времени с __ntp0.ntp-servers.net__
	11. Проверить доступность общей сетевой папки, проверить корректность выданных прав
	12. Подключить сетевую папку __MyShare__ как сетевой диск с меткой __Z__
	13. Создать свою папку в MyShare через командную строку. Каталог должен быть __\\\win-srv-lab01\MyShare\\<имя компьютера>\\<имя пользователя>__. Выполнять данную операцию только с помощью командной строки и использованием переменных окружения
	14. Подключить созданную на предыдущем шаге папку как сетевой диск с меткой __M__, подключение должно восстанавливаться при повторном входе в систему
	15. Разделить дисковое пространство на две части. Системный раздел должен иметь __70%__ пространства, другой раздел должен иметь __30%__. То есть, если диск 100 Гб, то системный раздел должен быть 70Гб, а дополнительный 30 Гб. Метку нового раздела использовать __F__
	16. Через __локальные групповые политики__ изменить экран блокировки системы и обои рабочего стола для __всех__ пользователей, картинки поместить в корень второго диска: 
        - В качестве блокировки использовать картинку - https://img.goodfon.ru/wallpaper/nbig/9/a5/msi-gs70-gs70stealth-keyboard.jpg
        - В качестве обоев - https://img.goodfon.ru/wallpaper/nbig/5/77/windows-10-logotip-windows-10.jpg
	17. Скопировать файл __enter_the_matrix.bat__ в __C:\Windows__. Создать задачу, чтобы __при каждом входе__ любого пользователя в систему запускался данный батник
	18. Выгрузить журнал логов системы с машины и сжать его в отдельный архив (установить предварительно __7-zip__). Архив положить на второй раздел жесткого диска
	19. Написать скрипт по установке программы __"Налогоплательщик ЮЛ"__ на данную ВМ (__Для тех, кому не лень__). Скрипт должен делать следующее:
		- Создать временную директорию для установочных файлов
		- Создать директорию для будущей программы
		- Установить утилиту __wget__ с помощью __curl__ (ссылка на wget - https://eternallybored.org/misc/wget/1.21.4/64/wget.exe)
		- Через wget получить установочный файл __.msi__ Налогоплательщика ЮЛ (установщик msi искать на сайте НП ЮЛ)
		- Установить программу в тихом режиме в созданную папку
		- Вывести ярлык программы для всех пользователей на рабочий стол

## Лабораторная работа №2. Конфигурация сети в Windows Server (DHCP, DNS, маршрутизация и NAT)
1. Создать дополнительную ВМ с Windows Server 2019 (использовать версию Standard __без GUI, редакция Core__). Дисковое пространство ВМ - __30 гб__, но можно и меньше.
2. Выполнить базовую настройку ВМ:
	1. Задать имя сервера __WIN-SRV-LAB02__
	2. Изменить часовой пояс на __Russia Standard Time__ (Москва +3)
	3. Задать статический IP-адрес серверу из той же подсети, что и предыдущие ВМ, в качестве DNS использовать __Яндекс__
	4. Разрешить серверу отвечать на __ICMP__
	5. Синхронизировать время с __ntp0.ntp-servers.net__
3. Добавить три сетевые карты к виртуальной машине. Один адаптер поместить в сеть __vmbr10__, другой в __vmbr20__, третий в __vmbr30__ (названия сетей/адаптеров может отличаться, в зависимости от того, в чем работаете)
4. Настроить интерфейсы следующим образом:
   
	__vmbr10__:
	- ip address: 10.0.10.1
	- netmask: 255.255.255.0
 
	__vmbr20__:
	- ip address: 10.0.20.1
	- netmask: 255.255.255.0

	__vmbr30__:
	- ip address: 10.0.30.1
	- netmask: 255.255.255.0 
5. Поменять адресацию на __WIN-SRV-LAB01__ на 10.0.10.10/24, шлюз - ip адрес __WIN-SRV-LAB02__, в качестве DNS-сервера использовать свой же ip-адрес
6. Подключить сервер __WIN-SRV-LAB02__ в консоль управления на сервере __WIN-SRV-LAB01__
   1. Добавить в доверенные сервер __LAB02__ через __WSman__
   2. Подключить сервер к управлению
   3. Установить на __WIN-SRV-LAB01__ модули удаленного администрирования (__Remote Access, DHCP, DNS__)
7. Установить роль __Remote Access__ на сервер __WIN-SRV-LAB02__
	1. Включить маршрутизацию
	2. Настроить NAT через интерфейс, смотрящий в Интернет
8. Установить на сервер __WIN-SRV-LAB02__ роль __DHCP__:
	1. Создать __два DHCP-пула__ со следующими параметрами:
   
  		Scope 1
      	- Name: VLAN20
     	- Scope Description: PC VLAN 20
     	- Network: 10.0.20.0/24
     	- Gateway: 10.0.20.1
     	- DNS: <ip адрес win-srv-lab01>
     	- Excluded addresses: 10.0.20.1-10
     	- Lease Time: 1d
  
  		Scope 2
    	- Name: VLAN30
    	- Scope Description: PC VLAN 30
    	- Network: 10.0.30.0/24
    	- Gateway: 10.0.30.1
    	- DNS: <ip адрес win-srv-lab01>
    	- Excluded addresses: 10.0.30.1-20
    	- Lease Time: 2d
  	1. Поместить машину __WIN-WD-LAB01__ в сеть __vmbr20__, переключить статику на DHCP и проверить получение адреса, проверить работу __NAT__ (открытие сайтов и icmp до 8.8.8.8 и 77.88.8.8)
  	2. Настроить резервацию по __мак-адресу__ для виртуальной машины клиента __WIN-WD-LAB01__
  	3. Создать ВМ __WIN-WD-LAB02__ и поместить её в сеть __vmbr_30__, проверить получение адреса по __DHCP__, проверить выход в Интернет (открытие сайтов и icmp до 8.8.8.8 и 77.88.8.8)
9. Установить на сервер __WIN-SRV-LAB01__ роль __DHCP__:
	1. Настроить __failover DHCP__. В качестве основного сервера DHCP должен выступать __WIN-SRV-LAB01__, в качестве резервного __WIN-SRV-LAB02__
	2. Режим феиловера - __Hot standby__, ключ аутентификации - __P@ssw0rd__, роль активного сервера должен выполнять __WIN-SRV-LAB01__
	3. Настроить на интерфейсах сети __vmbr20 и 30__ __DHCP-relay агент__
	4. Проверить отказоустойчивость путем отключения сервиса DHCP на сервере __LAB01__
	5. Если dhcp relay совсем не отрабатывает, то делаем Active-сервером __WIN-SRV-LAB02__. Так как у него есть все интерфейсы, то DHCP запрос до него гарантированно дойдет.
10.  Установить роль __DNS__ на сервер __WIN-SRV-LAB01__
   1. Создать первичную зону __skynet.local__ и запретить её динамическое обновление
   2. Настроить форвардинг DNS-запросов на DNS-сервер яндекса __77.88.8.8__
   3. Создать DNS-записи в зоне __skynet.local__:
    
    DNS-запись |IP-адрес 			    |Тип записи          
	|:---------:|:---------------------:|:------------------:|
	|win-srv-lab01|<ip адрес сервера>     |A-запись			|
 	|win-srv-lab02|<ip адрес сервера>     |A-запись			|
	|win-wd-lab01|<ip адрес компьютера>   |A-запись			|
 	|routesrv   |<ip адрес win-srv-lab02> |CNAME на win-srv-lab02|
 	|dnssrv     |<ip адрес win-srv-lab01> |CNAME на win-srv-lab01|
	|test       |10.0.10.100              |A-запись         |
	|test       |10.0.10.101              |A-запись         |

   5. Запретить использование кириллицы в DNS-записях
   6. Выключить поддержку алгоритма __Round-robin__
   7. Проверить доступность внешних ресурсов по DNS именам с клиентских машин (например: mail.ru, vk.com, youtube.com)
   8. Проверить через nslookup, что созданные записи разрешаются в ip-адреса (учитывать, что нужно писать полное имя записи)
   9. На сервере __WIN-SRV-LAB02__ исправить DNS на адрес сервера __WIN-SRV-LAB01__ и проверить, что записи зоны skynet.local разрешаются, а также, что работает перенаправление запросов на внешний DNS
---
## Материалы для выполнения лабораторных работ 1-2
#### Лабораторная 1
- [Установка и настройка Windows Server на примере версии 2022](https://pyatilistnik.org/installing-and-configuring-windows-server-2021/)
- [Установка и настройка Windows Server Core](https://pyatilistnik.org/install-and-configure-windows-server-2019-core/)
- [Файл hosts](https://windowsnotes.ru/other/tajna-fajla-hosts/)
- [DISM - управление компонентами образов Windows](https://ab57.ru/cmdlist/dism.html)
- [Использование nslookup](https://vmblog.ru/ispolzvanie-komandy-nslookup-v-windows/)
- [Обзор средства управления дисками](https://learn.microsoft.com/ru-ru/windows-server/storage/disk-management/overview-of-disk-management)
- [Локальные пользователи и группы](https://webistore.ru/administrirovaniye-windows/gruppy-polzovatelej-v-windows-lokalnye-polzovateli-i-gruppy/)
- [Управление локальными групповыми политиками](https://1cloud.ru/help/windows/upravlenie-lokalnymi-gruppovymi-politikami)
- [Права доступа к файлам и папкам простым языком](http://vasilisc.com/simple-permissions)
- [Планировщик заданий](https://pc.ru/docs/windows/task-scheduler)
- [Журнал событий](https://serverspace.ru/support/help/zhurnaly-windows/)
- [Общий доступ к папкам и файлам](https://winitpro.ru/index.php/2018/06/21/obshchij-dostup-k-setevym-papkam-i-printeram-bez-homegroup/)
- [Основы командной строки Windows](https://info-comp.ru/vseowindowsst/53-comandstroka.html)
- [Powershell для начинающих](https://tproger.ru/translations/powershell-tutorial)
- [Написание BAT-скриптов своими руками](https://selectel.ru/blog/bat-file/)

---
#### Лабораторная 2
- [Настройка DHCP failover](https://windowsnotes.ru/windows-server-2016/nastrojka-dhcp-failover-v-windows-server-2016/)
- [DHCP Relay в Windows Server](https://winitpro.ru/index.php/2013/08/22/nastrojka-dhcp-relay-agent-v-windows-server-2012/)
- [Server Manager и рабочие группы](https://windowsnotes.ru/windows-server-2012/windows-server-2012-nastrojka-server-manager-v-rabochix-gruppax/)
- [Настройка NAT](https://interface31.ru/tech_it/2010/02/windows-server-nastrojka-nat-dhcp.html)
- [Установка и настройка DHCP сервера](https://winitpro.ru/index.php/2015/11/10/nastrojka-dhcp-servera-s-pomoshhyu-powershell/)
- [Установка и настройка DNS сервера](https://serverspace.ru/support/help/configuring-a-dns-server-on-windows-server-2012-or-later/)
---
### Видеоматериалы
#### Общий материал
- [Виртуализация](https://www.youtube.com/watch?v=WWNfsRb5RSU&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=3&ab_channel=TrainITHard)
- [Сетевые сервисы](https://www.youtube.com/watch?v=IyuOLtZ8QM0&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=5&ab_channel=TrainITHard)
- [Разрешения файловой системы](https://www.youtube.com/watch?v=4b6ypDbGW3c&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=13&ab_channel=TrainITHard)
- [Разворачиваем тестовый стенд](https://www.youtube.com/watch?v=MtnukhCnK_4&list=PLUNgQQczUJbsLIiqvHVgy23nlwYnsEsn8&index=11&ab_channel=TrainITHard)

#### Основы Windows Server
##### TrainIT Hard
 - [Урок 2 - Server Manager, настройка](https://www.youtube.com/watch?v=h0veujXjZV0&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=2&ab_channel=TrainITHard)
 - [Урок 3 - DHCP сервер](https://www.youtube.com/watch?v=ymUf8mUTSaY&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=3&ab_channel=TrainITHard)
 - [Урок 4 - Основы DNS](https://www.youtube.com/watch?v=dXkC3L9IdEk&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=4&ab_channel=TrainITHard)

##### Зарубежный материал
 - [Windows Server Core Installation](https://www.youtube.com/watch?v=hzZPy4RhL-0&list=PLUZTRmXEpBy32NP6z_qvVBOTWUzdTZVHt&index=9&ab_channel=MSFTWebCast)
 - [Basic Configuration tasks in Windows Server 2019](https://www.youtube.com/watch?v=1nxYJSV7-u8&list=PLUZTRmXEpBy32NP6z_qvVBOTWUzdTZVHt&index=2&ab_channel=MSFTWebCast)


## Отчет по выполнению задания

#### Лабораторная работа №1
Результаты выполнения команд в виде скриншотов.
1. Результаты выполнения команд в __Powershell__ с машин __WIN-SRV-LAB01__ и __WIN-WD-LAB01__:
   - hostname
   - get-timezone
   - w32tm /query /source
   - ping win-srv-lab01, ping win-wd-lab01 (выполнять команды с обеих машин)
   - ipconfig /all
2. С машины __WIN-SRV-LAB01__ прислать следующие скриншоты:
   - Содержимое локальных групп и пользователей
   - Содержимое группы Administrators
   - Права на общую папку MyShare в свойствах папки
   - Рабочий стол с наличием браузера MS Edge
3. С машины __WIN-WD-LAB01__ прислать скриншоты:
   - Экран блокировки своего пользователя и администратора
   - Картинка рабочего стола своего пользователя и администратора
   - Скриншоты содержимого файла из команды gpresult /h result.html (также вложить сам файл result.html)
   - Запущенный скрипт матрицы при входе в систему, скриншот содержимого задачи из планировщика
   - telnet stepik.org 443
   - nslookup win-srv-lab01, nslookup it-learn.space
   - get-partition
   - net use под своим пользователем (у которого должны быть подключены диски), скриншот основного каталога __Этот компьютер__
   - Содержимое архива с логами
   - Код скрипта на установку налогоплательщика
  
#### Лабораторная работа №2
Результаты выполнения команд в виде скриншотов.
1. Выполнить systeminfo на WIN-SRV-LAB02 и прислать скриншот
2. На WIN-SRV-LAB02 выполнить команды:
    - hostname
    - get-timezone
    - ipconfig /all
    - nslookup it-learn.space
    - w32tm /query /source
    - Get-DhcpServerv4Scope
    - Get-DhcpServerv4ExclusionRange
    - Get-DhcpServerv4Reservation -ScopeID 10.0.20.0 (также для 10.0.30.0)
    - Get-DhcpServerv4Lease -ScopeId 10.0.20.0 (также для 10.0.30.0)
    - Get-DhcpServerv4Failover
3. Скриншот с сервера WIN-SRV-LAB01 из менеджера управления серверами, вкладка All Servers
4. На WIN-SRV-LAB01 выполнить следующие команды:
    - Get-DhcpServerv4Scope
    - Get-DhcpServerv4ExclusionRange
    - Get-DhcpServerv4Reservation -ScopeID 10.0.20.0 (также для 10.0.30.0)
    - Get-DhcpServerv4Lease -ScopeId 10.0.20.0 (также для 10.0.30.0)
    - Get-DhcpServerv4Failover
    - Get-DnsServerZone
    - Get-DnsServerForwarder
    - Get-DnsServerResourceRecord -ZoneName skynet.local
5. С машины WIN-SRV-LAB01 сделать скриншоты:
    - Выключенный алгоритм Round robin
	- Невозможность добавления DNS-записи с кириллицей (скриншот ошибки при добавлении)
6. На машине WIN-WD-LAB01 выполнить команды и сделать скриншоты:
    - ipconfig /all
    - nslookup yandex.ru
	- nslookup win-srv-lab01.skynet.local
	- nslookup win-srv-lab01
	- ping -n 1 -4 -w 1 win-wd-lab01
	- ping -n 1 -4 -w 1 win-wd-lab01.skynet.local
	- ping -n 1 -4 -w 1 win-srv-lab01.skynet.local
	- ping -n 1 -4 -w 1 win-srv-lab01
	- Выполнить 3 раза подряд команду ping -n 1 -w 1 -4 test.skynet.local
7. Прислать скриншот с WIN-WD-LAB01 открытых сайтов в интернете (например, vk.com)