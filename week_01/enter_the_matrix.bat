@echo off
rem Настройки
chcp 65001
title Matrix
setlocal enabledelayedexpansion
for /f %%e in ('echo prompt $e^| cmd') do @set "esc=%%e" %== Escape-последовательности ==%
rem См. https://docs.microsoft.com/ru-ru/windows/console/console-virtual-terminal-sequences
set "col=80" %== столбцов в консоли ==%
set "row=25" %== строк в консоли ==%
mode con cols=%col% lines=%row% %== ставим режим консоли ==%
set "rain_length=11" %== длина цепочки ==%
for /l %%y in (1,1,%col%) do set "d_col[%%y]= " %== что показывать ==%
for /l %%y in (1,1,%col%) do set "cnt_col[%%y]=0" %== счётчик длины цепочки ==%
<nul set /p "=%esc%[?25l" %== спрятать курсор ==%
cls
rem Главный цикл 
:matrix_loop
for /l %%y in (1,1,%col%) do (
 if !cnt_col[%%y]! equ 0 (
  set "d_col[%%y]= "
 ) else (
  set /a "rnd_digit=!random! %% 10"
  if !cnt_col[%%y]! equ 1 (
   set "d_col[%%y]=%esc%[97m!rnd_digit!%esc%[32m"
  ) else if !cnt_col[%%y]! equ 2 (
   set "d_col[%%y]=%esc%[92m!rnd_digit!%esc%[32m"
  ) else (
   set "d_col[%%y]=!rnd_digit!"
  )
  set /a "cnt_col[%%y]=(!cnt_col[%%y]! + 1) %% (%rain_length% + 1)"
 )
 set /a "n_drop=!random! %% 22" %== чем больше число, тем реже дождь ==%
 if !n_drop! equ 0 set "cnt_col[%%y]=1"
)
set "d_line=%esc%[32m"
for /l %%y in (1,1,%col%) do set "d_line=!d_line!!d_col[%%y]!"
<nul set /p "=%esc%[1T%esc%[1;1H" %== прокрутить вниз и поставить курсор ==%
echo %d_line%
goto matrix_loop