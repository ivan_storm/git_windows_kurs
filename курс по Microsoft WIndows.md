# Что будет в курсе по администрированию Microsoft Windows?

Курс ориентирован на общее знакомство с серверной операционной системой Microsoft Windows. Будут рассмотрены базовые технологии, без которых не обходится ни одна крупная компания сегодня. На протяжении всего обучения поэтапно будет выстраиваться готовая инфраструктура на базе Microsoft. Понимание инфраструктуры Microsoft позволит лучше понять как устроена инфраструктура, что такое DNS, DHCP, домен и т.д.

# Топология инфраструктуры Microsoft
![Топология инфраструктуры](https://gitlab.com/ivan_storm/git_windows_kurs/raw/main/windows_scheme_2.0.png)

# Порядок выполнения лабораторных работ
* 1 неделя - Лабораторные 1 и 2
* 2 неделя - Лабораторные 3 - 5
* 3 неделя - Лабораторные 6-8
* 4 неделя - Лабораторная 9-10


# Лабораторная работа №1. Знакомство с Windows Server и углубленная работа с Windows 10
1. Создать две ВМ, одну с Windows Server 2019 (использовать версию __Standard с GUI__), другую Windows 10 LTSC, гипервизор любой. Желательно использовать Proxmox для возможности создания шаблона и удобного клонирования виртуальных машин в дальнейшем. Дисковое пространство каждой ВМ по __45гб__ (если мало места на диске, то можно уменьшить до 30гб)
2. На Windows Server 2019 выполнить следующие настройки:
	1. Задать имя сервера __WIN-SRV-LAB01__
  	2. Изменить часовой пояс на __Russian Standard Time__ (Москва +3)
  	3. Отключить защиту в __Internet Explorer__
  	4. Задать статический IP-адрес серверу (сетевой адаптер ВМ настроить в bridge и задать параметры от домашнего роутера), в качестве сервера DNS использовать Яндекс __77.88.8.8__
  	5. Разрешить серверу отвечать на ICMP (пинг)
  	6. Создать пользователя со своим именем (например, Ivan) с паролем __P@ssw0rd__, добавить пользователя в группу локальных администраторов
  	7. Установить браузер __Microsoft Edge__
  	8. В корне диска C создать каталог __MyShare__ и предоставить общий доступ своему пользователю. Права на __чтение и запись__.
  	9. Выполнить синхронизацию времени с __ntp0.ntp-servers.net__
3. На Windows 10 выполнить следующие настройки:
	1. Задать имя компьютера __WIN-WD-LAB01__
	2. Изменить часовой пояс на __Russia Standard Time__ (Москва +3)
	3. Включить встроенную учетную запись администратора (__Администратор__), задать пароль __P@ssw0rd__
	4. Создать пользователя со своим именем с паролем __P@ssw0rd__, добавить его в группу локальных администраторов и залогиниться под ним
	5. Задать статический ip-адрес из той же подсети, что и Windows-сервер (DNS Яндекса)
	6. В файле __hosts__ прописать DNS-запись для установленного Windows-сервера. Имя записи должно совпадать с именем Windows-сервера
	7. Включить __Telnet-клиент__ в системе через командную строку с применением Powershell или утилиты DISM
	8. Проверить доступность Windows-сервера по имени через утилиту __ping__
	9. Проверить телнетом доступность веб-сайта __atomskills.tech__ по __443__ порту
	10. Выполнить синхронизацию времени с __ntp1.ntp-servers.net__
	11. Проверить доступность общей сетевой папки, проверить корректность выданных прав
	12. Подключить сетевую папку __MyShare__ как сетевой диск с меткой __Z__
	13. Создать свою папку в MyShare через командную строку. Каталог должен быть __\\\win-srv-lab01\MyShare\\<имя компьютера>\\<имя пользователя>__. Выполнять данную операцию только с помощью командной строки и использованием переменных окружения
	14. Подключить созданную на предыдущем шаге папку как сетевой диск с меткой __M__, подключение должно восстанавливаться при повторном входе в систему
	15. Разделить дисковое пространство на две части. Системный раздел должен иметь __35 гб__ пространства, другой раздел должен иметь __10 гб__. Метку раздела использовать __F__
	16. Через __локальные групповые политики__ / __редактор реестра__ изменить экран блокировки системы и обои рабочего стола для __всех__ пользователей, картинки поместить в корень второго диска: 
        - В качестве блокировки использовать картинку - https://img.goodfon.ru/wallpaper/nbig/9/a5/msi-gs70-gs70stealth-keyboard.jpg
        - В качестве обоев - https://img.goodfon.ru/wallpaper/nbig/5/77/windows-10-logotip-windows-10.jpg
	17. Скопировать файл __enter_the_matrix.bat__ в __C:\Windows__. Создать задачу, чтобы __при каждом входе__ любого пользователя в систему запускался данный батник
	18. Выгрузить журнал логов системы с машины и сжать его в отдельный архив (установить предварительно __7-zip__). Архив положить на второй раздел жесткого диска
	19. Написать скрипт по установке программы __"Налогоплательщик ЮЛ"__ на данную ВМ (__Для тех, кому не лень__). Действовать по следующему алгоритму:
		- Создать временную директорию для установочных файлов
		- Создать директорию для будущей программы
		- Установить утилиту __wget__ с помощью __curl__ (ссылка на wget - https://eternallybored.org/misc/wget/1.21.4/64/wget.exe)
		- Через wget получить установочный файл __.msi__ Налогоплательщика ЮЛ (установщик msi искать на сайте НП ЮЛ)
		- Установить программу в тихом режиме в созданную папку
		- Вывести ярлык программы для всех пользователей на рабочий стол

# Лабораторная работа №2. Конфигурация сети в Windows Server (DHCP, DNS, маршрутизация и NAT)
1. Создать дополнительную ВМ с Windows Server 2019 (использовать версию Standard __без GUI, редакция Core__). Дисковое пространство ВМ - __35 гб__.
2. Выполнить базовую настройку ВМ:
	1. Задать имя сервера __WIN-SRV-LAB02__
	2. Изменить часовой пояс на __Russia Standard Time__ (Москва +3)
	3. Задать статический IP-адрес серверу (сетевой адаптер ВМ настроить в bridge и задать параметры от домашнего роутера), в качестве сервера DNS использовать Яндекс __77.88.8.8__
	4. Разрешить серверу отвечать на __ICMP__ (пинг)
	5. Синхронизировать время с __ntp1.ntp-servers.net__
3. Добавить две сетевые карты к виртуальной машине. Один адаптер поместить в сеть __vmbr10__, другую в __vmbr20__
4. Настроить интерфейсы следующим образом:
   
	__vmbr10__:
	- ip address: 10.0.10.1
	- netmask: 255.255.255.0
 
	__vmbr20__:
	- ip address: 10.0.20.1
	- netmask: 255.255.255.0

	__vmbr30__:
	- ip address: 10.0.30.1
	- netmask: 255.255.255.0 
5. Подключить сервер __WIN-SRV-LAB02__ в консоль управления на сервере __WIN-SRV-LAB01__
   1. Добавить в доверенные сервер __LAB02__ через __WSman__
   2. Подключить сервер к управлению
   3. Установить на __WIN-SRV-LAB01__ модули удаленного администрирования (__Remote Access, DHCP, DNS__)
6. Установить роль __Remote Access__ на сервер __WIN-SRV-LAB02__
	1. Включить маршрутизацию
	2. Настроить NAT через основной интерфейс с __\<Ip адресом, полученного от домашнего роутера>__
7. Установить на сервер __WIN-SRV-LAB02__ роль __DHCP__:
	1. Создать __два DHCP-пула__ со следующими параметрами:
   
  		Scope 1
      	- Name: VLAN20
     	- Scope Description: PC VLAN 20
     	- Network: 10.0.20.0/24
     	- Gateway: 10.0.20.1
     	- DNS: <внешний Ip-адрес сервера>
     	- Excluded addresses: 10.0.20.1-10
     	- Lease Time: 1d
  
  		Scope 2
    	- Name: VLAN30
    	- Scope Description: PC VLAN 30
    	- Network: 10.0.30.0/24
    	- Gateway: 10.0.30.1
    	- DNS: <внешний Ip адрес сервера>
    	- Excluded addresses: 10.0.30.1-20
    	- Lease Time: 2d
  	1. Поместить машину __WIN-WD-LAB01__ в сеть __vmbr20__, проверить получение адреса по __DHCP__, проверить работу __NAT__
  	2. Настроить резервацию по __мак-адресу__ для виртуальной машины клиента __WIN-WD-LAB01__
  	3. Создать ВМ __WIN-WD-LAB02__ и поместить её в сеть __vmbr_30__, проверить получение адреса по __DHCP__, проверть выход в Интернет
8. Установить на сервер __WIN-SRV-LAB01__ роль __DHCP__:
	1. Настроить __failover DHCP__. В качестве основного сервера DHCP должен выступать __WIN-SRV-LAB01__, в качестве резервного __WIN-SRV-LAB02__
	2. Режим феиловера - __Hot standby__, роль партнера - __Standby__, ключ аутентификации - __P@ssw0rd__
	3. Настроить на интерфейсах VLAN20 и 30 __DHCP-relay агент__
	4. Проверить отказоустойчивость путем отключения сервиса DHCP на сервере LAB01
9.  Установить роль __DNS__ на сервер __WIN-SRV-LAB01__
   1. Создать первичную зону __skynet.local__ и запретить её динамическое обновление
   2. Настроить форвардинг DNS-запросов на DNS-сервер яндекса __77.88.8.8__
   3. Создать DNS-записи в зоне __skynet.local__:
    
    DNS-запись |IP-адрес 			    |Тип записи          
	|:---------:|:---------------------:|:------------------:|
	|win-srv-lab01|<ip адрес сервера>     |A-запись			|
 	|win-srv-lab02|<ip адрес сервера>     |A-запись			|
 	|routesrv   |<ip адрес win-srv-lab02> |CNAME на win-srv-lab02|
 	|dnssrv     |<ip адрес win-srv-lab01> |CNAME на win-srv-lab01|

   4. Запретить использование кириллицы в DNS-записях
   5. Выключить поддержку алгоритма __Round-robin__
   
 # Лабораторная работа №3. Основы Active Directory и управления объектами
 1. Установить службу каталогов Active Directory на сервер WIN-SRV-LAB01
    1. Имя домена - __grenlab.local__
    2. Пароль администратора домена __P@ssw0rd__, пароль для восстановления каталогов такой же
 2. Создать главную организационную единицу с именем домена grenlab, а в ней следующие организационные единицы в AD:
 	- MSK
 	- NN
 	- SPB
 	- EKB
 	- свой город (название для контейнера на свое усмотрение, например, мой город Заволжье (Zavolzhye) - ZVL)
  
 	В каждом контейнере создать следующие:
 	- __Admins__ - контейнер для хранения объектов, связанных с администрированием
 	- __Computers__ - контейнер для хранения объектов типа компьютера
 	- __Disabled Users__ - контейнер для хранения отключенных объектов типа пользователи
 	- __Groups__ - контейнер для хранения объектов типа группы
 	- __Printers__ - контейнер для хранения объектов типа пользователи и группы для принтеров
 	- __Users__ - контейнер для хранения объектов типа пользователи

	Для выполнения данного задания рекомендуется написать скрипт на Powershell.
 3. Создать своего пользователя в __Active Directory__:
    1. Логин учетной записи (УЗ) должен быть в формате __<Первый символ имени>__ + __<Первый символ отчества>__ + __<Фамилия>__, например __SMIvanov__
    2. Отображаемое имя должно соответствовать __полным ФИО__
    3. Пароль должен быть P@ssw0rd_23
    4. В дополнительных атрибутах пользователя добавить свой номер мобильного телефона, город проживания и должность (например, студент)
    5. УЗ поместить в контейнер своего города
    6. Добавить __свою УЗ__ в группу доменных администраторов
 4. Создать группы безопасности в каждой организационной единице в формате __\<OU>-SharedFolders-Users__, __\<OU>-PrintersAccess__ и __\<OU>-City-Users__
 5. Создать контейнер Groups в grenlab, в Groups создать группы __CertAutoEnroll-Users__, __Grenlab-WD-Admins__ и __NetworkDevice-MGMT-Admins__. В группу __NetworkDevice-MGMT-Admins__ и __Grenlab-WD-Admins__ добавить своего пользователя
 6. Создать контейнер __Servers__ в __grenlab__
 7. Создать учетные записи из __CSV-файла__:
    1. Каждый пользователь должен попасть в соответствущий контейнер и группу
    2. Пароль для всех пользователей использовать __P@ssw0rd__
    3. Данные должны заполниться согласно файлу. Необходимо применить PowerShell.
 8. На клиентских компьютерах:
    1. удалить все лишние записи из файла __hosts__
    2. Ввести компьютер __WIN-WD-LAB01__ в домен
    3. Перенести объект в контейнер __MSK->Computers__
    4. Залогиниться под своим пользователем, потом под любым созданным пользователем
    5. Аналогичные действия сделать с хостом __WIN-WD-LAB02__
 9.  Настроить парольную политику для группы __NetworkDevice-MGMT-Admins__ следующим образом:
 	- Минимальное кол-во символов - __10__
 	- спецсимволы, разный регистр
 	- смена пароля раз в __15 дней__
 	- минимальное время для смены пароля __1 день__
 	- Число запоминаемых паролей 6
 10. Включить корзину в __Active Directory__
     1.  В контейнере grenlab создать три объекта типа компьютер с именами __TEMP-WD-01-3__ и удалить их
     2.  Восстановить объект __TEMP-WD-02__ в контейнер __grenlab__ в __Restored__
 11. Создать и настроить виртуальную машину с Windows Server 2019:
     1.  IP-адрес из сети __10.0.10.0/24__, DNS - домен контроллер
     2.  Имя сервера __WIN-SRV-LAB03__
     3.  Ввести сервер в домен __grenlab.local__, переместить объект в ранее созданный контейнер __Servers__
     4.  Добавить сервер в менеджер управления серверами на __WIN-SRV-LAB01__
 12. Создать и настроить виртуальную машину с Windows Server 2019 (__ВНИМАНИЕ! Для тех, у кого мало ресурсов можно выключить двух клиентов и WIN-SRV-LAB03__):
     1.  IP-адрес из сети __10.0.40.0/24__, DNS - домен контроллер
     2.  Имя сервера __WIN-SRV-LAB04__
     3.  Ввести сервер в домен __grenlab.local__
     4.  Установить роль __Active Directory__
     5.  Сделать данный сервер __Read Only Domain Controller__
 
 # Лабораторная работа №4. Общий доступ к файлам в Windows Server, FSRM, пространство DFS (общий доступ, разграничение прав, RAID-массив, DFS, квоты)
 1. Установить на сервер __WIN-SRV-LAB01__ службы __FSRM__ и __DFS__
 2. Создать два сетевых диска на __WIN-SRV-LAB01__
    1. Добавить 4 жестких диска к виртуальной машине по __5 ГБ__
    2. Собрать каждые два диска в __RAID 1__ массивы (__RAID 1__ это зеркалирование, по итогу из двух дисков отобразится подключенным 1)
    3. Таблицу разделов использовать __GPT__, файловая система __NTFS__
    4. Логическим разделам задать метки __S__ и __K__
    5. Метки волюмов - __Net Disk__
 3. Создать следующую структуру папок:
	- Home_Folders (Диск S)
	- Shared_Folders (Диск K)
	- Soft_Folders (Диск K)
	- City_Folders (Диск S)
	- Documents_Folders (Диск C __WIN-SRV-LAB03__)
	- Photo_Folders (Диск C __WIN-SRV-LAB03__)
 4. Выдать права на каталоги следующим образом:
    - Home_Folders -> __Domain Users__, разрешить создание папок внутри каталога, в самой папке владельцу можно делать что угодно, у остальных пользователей доступа быть не должно (кроме администраторов)
    - Shared_Folders -> __Domain Users__, только чтение, __Shared_Folders_Admins__ - полные права
    - Soft_Folders -> __Shared_Folders_Admins__, полные права, __Domain Users__ - чтение
    - City_Folders -> __\<OU>-SharedFolders-Users__, права на чтение, полные права - у админов, у других прав на просмотр быть не должно
   1. Настроить общий доступ ко всем каталогам
 5. Создать квоты на каталоги следующим образом:
	- Home_Folders - каждому пользователю по __250 Мб__
	- Shared_Folders - общая квота __2 Гб__
 6. Создать пространство __DFS__ 
    1. Создать пространство __DFS__, назвать __Grenlab_DFS__
    2. Связать следующие папки следующим образом:
       1. Общие папки -> Shared_Folders
       2. Программное обеспечение -> Soft_Folders
       3. Документы -> Documents_Folders (__с WIN-SRV-LAB03__)
       4. Фотографии -> Photo_Folders (__с WIN-SRV-LAB03__)

 7. Реализовать __Home Folders__ для доменных пользователей
    1. В качестве сетевой шары использовать __Home_Folders__
    2. У каждого пользователя должна быть своя папка и доступ только к ней
    3. Домашний каталог необходимо прописать в соответствующем поле пользователя в AD

# Лабораторная работа №5. Групповые политики
1. Создать следующие групповые политики:
    1. Отключить __приветственную анимацию__ на всех клиентских ПК. Имя политики __DisableFirstSignAnimation__
    2. Включить на всех клиентских ПК возможность запуска от имени другого пользователя в меню __Пуск__. Имя политики __StartMenuProgramFromOtherUser__
    3. Блокировка компьютера при простое __3 минуты__, отключить __заставку__ и требовать __пароль__ при разблокировке. Имя политики __LockPCAfter5Minutes__
    4. Добавление пользователей группы __Grenlab-WD-Admins__ в локальную группу администраторов. Имя политики __LocalAdmins__
    5. В политики домена __по умолчанию__ включить параметр __Всегда ждать запуска сети при запуске и входе в систему__ (Если не включить, то софт в следующих пунктах не сможет установиться)
    6. Установить браузер __Mozilla Firefox__ через политики на все компьютеры и сделать его браузером по умолчанию (использовать файл ассоциаций для установки ПО по умолчанию). Имя политики __FirefoxInstall__
    7. Установить __Notepad++__ на все компьютеры. Имя политики __NotepadPPInstall__
    8. Распространить следующие ярлыки на рабочий стол:
		- Telegram Web (ссылка на веб-версию телеграм, использовать оригинальную иконку)
		- Общие папки (каталог __Shared_Folders__)
		- __\<City>__ (каталог из __City_Folders__, использовать нацеливание на группу)
  	9. Разрешить в __Windows Firewall__ (для доменного профиля) полностью __ICMP__, __SSH__, запретить доступ до следующих сайтов по __http/https__ (имя политики __FirewallConfiguration__):
		- it-learn.space (также запретить 22 порт)
		- vk.com
		- mail.ru
	9.  Настроить часовой пояс Екатеринбург для офиса в Екатеринбурге (__применение политик по сайтам__)
	10. Подключение сетевого диска __DFS__ всем доменным пользователям с меткой __D__
	11. Разрешить пользователям группы __CertsAutoEnroll-GrenlabUsers__ автоматический выпуск сертификатов. Имя политики __CertAutoEnrollment__
	12. На контейнер __Servers__ применить политику по смене фона рабочего стола. Картинку использовать на свое усмотрение
2. Проверить результаты применения групповых политик на клиентских ПК

# Лабораторная работа №6. Настройка сайта, доступ по FTP, WebDAV
1. Установить на сервер __WIN-SRV-LAB03__ роль IIS
2. Создать веб-сайт и настроить к нему доступ:
   1. Имя сайта - __grensite.grenlab.local__
   2. Создать DNS-запись __grensite__ на сервере в зоне __grenlab.local__
   3. В качестве каталога для сайта использовать __C:\grenlabsite__
   4. В каталог с сайтом разместить следующие архив __site_01.zip__
   5. Доступ к сайту должен быть разрешен только из внутренней подсети основного офиса и только по доменному имени
3. Установить и настроить __FTP-сервер__:
   1. В качестве каталога использовать папку __C:\ftp__
   2. В каталог распаковать следующий архив __ftp.zip__
   3. Сделать доступ на чтение и редактирование локальной группе __ftp_users__. В эту группу должны входить доменные пользователи __NetworkDevice-MGMT-Admins__
   4. Ресурс должен быть доступен по имени __ftp.grenlab.local__
   5. Анонимный доступ должен быть отключен
   6. Межсетевой экран Windows должен быть настроен на работу с FTP
   7. Подключение должно быть разрешено только из серверной подсети и с хоста __WIN-WD-LAB02__
   8. С хоста __WIN-WD-LAB02__ загрузить какой-нибудь файл на FTP-сервер
4. Установить и настроить роль __WebDAV__
   1. Создать каталог __C:\webdav__
   2. Создать группу в AD __Grenlab-WebDAV-Users__ и добавить туда следующих пользователей:
      - Свой пользователь
      - IAPopov
      - KVLisin
      - NLKlopova
      - KASmotrin
   3. Создать локальную группу __webdav-users__ и добавить в нее ранее созданную группу AD
   4. Разрешить доступ к папке только пользователям ранее __созданной группы__
   5. Использовать __Basic__ аутентификацию
   6. Поместить в папку любые текстовые файлы
5. Создать второй сайт и настроить к нему доступ
   1. Создать каталог __C:\grencerts__
   2. Настроить возможность просматривать файлы в каталоге, положить туда какие-нибудь файлы
   3. Сайт должен быть доступен по имени __certs.grenlab.local__ только по __http__
   4. Включить поддержку __DoubleEscaping__
   
# Лабораторная работа №7. Центры сертификации в Windows Server
1. Установить роль __AD Certification Authority__ на сервер __WIN-SRV-LAB01__
   1. Имя центра сертификации - __Grenlab-RootCA__
   2. Срок действия сертификата - __3 года__
   3. Сертификат подчиненого CA должен действовать __1 год__
2. Установить __SubCA__ на сервер __WIN-SRV-LAB03__
   1. Имя подчиненного центра сертификации - __Grenlab-SubCA__
   2. В выпускаемых сертификатах должны присутсовать ссылки на списки отзыва сертификатов (CRL, Delta CRL), также должна присутствовать ссылка на корневой сертификат
   3. Списки отзыва должны быть размещены по пути http://certs.grenlab.local/crl, а корневой сертификат - http://certs.grenlab.local/aia
   4. Сертификаты должны без проблем скачиваться на хосты (помним про спецсимволы в файлах)
3. Создать шаблоны сертификатов для следующих ресурсов:
   1. Веб-сайты
      - Имя шаблона - Web-Sites
      - Subject Name - Supply the request
      - Security - Domain Admins, Authenticated Users (Enroll)
      - Validaty Period - 1 год
      - Разрешить экспортировать приватный ключ
   2. Клиентские машины
      - Имя шаблона - WD-Clients
      - Validaty Period - 6 месяцев
      - Subject Name - брать информацию из Active Directory, формат имени - Common name, также включать информацию - DNS и UPN
      - Security - Domain Computers, autoenrollment разрешен
4. Выпустить следующие сертификаты:
   1. Сайт __grensite.grenlab.local__
      - Common Name - grensite.grenlab.local
      - DNS - grensite.grenlab.local, grensite
   2. Портал __rds.grenlab.local__
      - Common Name - rds.grenlab.local
      - DNS - rds.grenlab.local, rds
   3. Сделать GPO для автоматического выпуска сертификата компьютерам (серт должен выпускаться автоматически)
5. Привязать к сайту ранее выпущенный сертификат, сделать редирект с __http__ на __https__ через модуль __[URL rewrite](https://iis-umbraco.azurewebsites.net/downloads/microsoft/url-rewrite)__
6. Проверить доступность сайта __grensite.grenlab.local__ по __https__, сертификат должен быть доверенным. В Firefox отдельно добавить корневой сертификат в доверенные
7. Отозвать выпущенный для сайта сертификат
   1. Причину отзыва выбрать 
   2. Убедиться, что Internet Explorer выдает сообщение о том, что сертификат отозван
7. Проверить работу __FTP и WebDAV__ 
8. Проверить, что корневой сертификат появился на доменных ПК и серверах

 # Лабораторная работа №8. Настройка RDS
 1. Установить на сервер __WIN-SRV-LAB03__ роль __RDS__ (сервер лицензий не ставить)
   1. Использовать сценарий развертывания __Session-based desktop deployment__
   2. Все компоненты должны находиться на данном сервере
   3. Настроить доступ к __RDS-Web__ по доменному имени __rds.grenlab.local__
   4. Настроить перенаправление с __http__ на __https__ (через http редирект, либо через url rewrite)
 2. Создать коллекцию с именем __RDS-Applications__
   1. Доступ к коллекции __RDS-Applications__ должен быть для всех __доменных пользователей__
   2. Создать две группы безопасности в глобальном OU __grenlab__ - __RDS-Putty-Users__ и __RDS-Paint-Users__, добавить в группу __RDS-Putty-Users__ всех сетевых администраторов, а также любого пользователя на свое усмотрение. В группу __RDS-Paint-Users__ добавить 4-х пользователей на свое усмотрение. 
   3. В коллекции опубликовать приложение __WordPad__, сделать его доступным всем пользователям коллекции.
   4. Опубликовать __Paint__ для пользователей групы __RDS-Paint-Users__, остальным пользователям приложение отображаться не должно
   5. Установить на сервер программу __Putty__ и опубликовать её группе __RDS-Putty-Users__
 3. Настроить __SSO__ только для запуска приложений RDS (применить GPO, имя политики __RDS_SSO__)
   1. Вход на сам веб-портал должен быть с указанием УЗ
   2. Запуск приложений через __Internet Explorer__ должен происходить при клике мышкой на __иконку приложения__
   3. Не должно всплывать никаких предупреждений и окон безопасности при запуске RDP файла
 4. Создать групповую политику для публикации всем доменным пользователям __ярлыка на портал RDS__
   1. Название политики - __RDSWebShortcut__
   2. Название ярлыка - __RDS Web Portal__
   3. Ярлык должен открывать портал RDS через __Internet Explorer__
 5. Проверить наличие ярлыка, доступ к порталу __RDS Web__ и работоспособность опубликованных приложений под пользователями


# Лабораторная работа №9. Настройка WDS.
1. Установить на сервер __WIN-SRV-LAB03__ роль __WDS__
2. Выполнить настройку __WDS__:
   1. Настроить работоспособность службы WDS
   2. Добавить образ __Windows 10__ для загрузки по сети
   3. Скорректировать настройки __DHCP__
   4. Обеспечить ввод компьютера в домен при инсталляции через WDS
   5. Установка должна быть в автоматическом режиме с использованием файлов ответов (для тех, кому не лень)
3. Создать пустую ВМ и накатить на нее систему через __WDS__

# Лабораторная работа №10. Настройка NPS-сервера
1. Установить на сервер __WIN-SRV-LAB03__ роль __NPS__
2. Импортировать ВМ с Cisco
3. Настроить устройство Cisco
   1. Переименовать хост
   2. Назначить ip-адрес для интерфейса управления
   3. Создать локального пользователя admin с максимальными привилегиями и паролем __P@ssw0rd__
   4. Включить поддержку __AAA__, подключить радиус-сервер (это сервер __WIN-SRV-LAB03__, ключ исползовать __cisco__)
   5. Настроить вход по консоли только под локальной учетной записью
   6. Включить поддержку __SSH версии 2__
   7.  Настроить аутентификацию и авторизацию через NPS-сервер
4. Добавить в NPS клиент Cisco, использовать тот же ключ, что указывали на сетевом устройстве
5. Сконфигурировать политику доступа к сетевому оборудованию:
   1. Для пользователей группы __NetworkDevice-MGMT-Admins__ вход должен быть по умолчанию с максимальным уровнем привилегий
6. Подключиться с любой машины по SSH на устройство Cisco. Вход под доменной УЗ, состоящей в группе __NetworkDevice-MGMT-Admins__ должен быть успешен и с максимальными привилегиями