# Материалы для выполнения лабораторных работ 3-5
---
### Текстовый материал
#### Лабораторная 3
- [Установка и настройка Windows Server на примере версии 2022](https://pyatilistnik.org/installing-and-configuring-windows-server-2021/)
- [Active Directory от А до Я](https://pyatilistnik.org/vvedenie-v-osnovnyie-ponyatiya-active-directory/)
- [Обзор доменных служб Active Directory. Документация Microsoft](https://learn.microsoft.com/ru-ru/windows-server/identity/ad-ds/get-started/virtual-dc/active-directory-domain-services-overview)
- [Установка Active Directory](https://itproblog.ru/%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-%D0%B8-%D0%BF%D0%B5%D1%80%D0%B2%D0%BE%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA/)
- [Список инструментов по управлению Active Directory](https://pyatilistnik.org/active-directory-management-administrative-tools/)
- [Создание и управление сайтами AD](https://vmblog.ru/sajty-podseti-active-directory/)
- [Создание пользователей в Active Directory](https://winitpro.ru/index.php/2018/03/02/new-aduser-sozdaem-polzovatelej-v-domene/)
- [Редактор атрибутов Active Directory](https://winitpro.ru/index.php/2019/08/15/active-directory-attribute-editor/)
- [Делегирование административных полномочий в Active Directory](https://winitpro.ru/index.php/2018/12/29/active-directory-delegirovanie-admin-prav/)
- [Используем Powershell для работы с AD](https://winitpro.ru/index.php/2019/07/18/modul-active-directory-dlya-powershell/)
- [Импорт данных из CSV файлов в PowerShell](https://winitpro.ru/index.php/2022/10/13/import-csv-fajlov-v-powershell/)
- [Как добавить компьютер в домен Active Directory](https://winitpro.ru/index.php/2022/10/06/dobavit-kompyuter-windows-v-domen-ad/)
- [Корзина Active Directory](https://habr.com/ru/companies/veeam/articles/324246/)
- [Контроллер домен Read-Only (RODC)](https://winitpro.ru/index.php/2010/11/21/rabota-s-kontrollerami-domena-read-only-rodc-chast-1/)
- [Установка и настройка RODC](https://winitpro.ru/index.php/2017/11/08/ustanovka-rodc-kontrollera-domena-na-windows-server-2016/)
---
#### Лабораторная 4
- [Настройка программного RAID](https://internet-lab.ru/software_raid_windows_server_2016)
- [Общий доступ к файлам и папкам](https://coderlessons.com/tutorials/microsoft-technologies/izuchite-windows-server-12/windows-server-2012-obshchii-dostup-k-failam)
- [Диспетчер ресурсов файлового сервера FSRM](https://wiki.merionet.ru/articles/ustanovka-i-nastrojka-dispetchera-resursov-fajlovogo-servera-file-server-resource-manager-v-windows-server-2016)
- [Динамический контроль доступа: списки свойств ресурсов и классификация файлов](https://habr.com/ru/articles/204212/)
- [Home Folders in Active Directory](https://sanuja.com/blog/how-to-create-home-folders-in-active-directory)
- [Distributed File System. Архитектура и базовые понятия](https://windowsnotes.ru/activedirectory/distributed-file-system-arxitektura-i-bazovye-ponyatiya/)
- [Установка и настройка DFS и репликации файлов](https://winitpro.ru/index.php/2013/09/25/ustanovka-i-nastrojka-dfs-i-replikacii-fajlov-v-windows-server-2012/)

#### Лабораторная 5
- [Что такое групповые политики (GPO)](https://habr.com/ru/companies/galssoftware/articles/543588/)
- [Управление групповыми политиками](https://1cloud.ru/help/windows/gruppovye-politiki-active-directory)
- [Установка программ через GPO](https://winitpro.ru/index.php/2011/10/21/ustanovka-programm-s-pomoshhyu-gruppovyx-politik/)
- [Почему не применяются GPO](https://winitpro.ru/index.php/2019/03/18/primenenie-gpo-spravka-admina/)
- [Создание ярлыков через GPO](https://winitpro.ru/index.php/2019/12/04/sozdat-yarlyk-na-rabochem-stole-gpo/)
- [Подключение сетевых дисков через GPO](https://winitpro.ru/index.php/2020/07/14/podklychit-setevoy-disk-windows-gpo/)
- [Настройка фаерволла через GPO](https://winitpro.ru/index.php/2018/12/06/nastrojka-pravil-windows-firewall-gpo/)
---
### Видеоматериалы
#### Общий материал
- [Основы Powershell](https://info-comp.ru/sisadminst/546-windows-powershell-basics.html)
- [Работы с массивами в Powershell](https://fixmypc.ru/post/rabota-v-powershell-s-massivami-i-listami-na-primerakh/)

#### Основы Windows Server
##### TrainIT Hard
 - [Урок 5 - Основы Active Directory Domain Services](https://www.youtube.com/watch?v=aytPzZ0djmQ&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=6&ab_channel=TrainITHard)
 - [Урок 6 - Advanced DNS, интеграция AD DS и DNS](https://www.youtube.com/watch?v=eKq7xROGpYo&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=6&ab_channel=TrainITHard)
 - [Урок 7 - Сайты и репликация Active Directory](https://www.youtube.com/watch?v=45FzcvixuZs&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=7&ab_channel=TrainITHard)
 - [Урок 8 - Доверие и группы безопасности в AD](https://www.youtube.com/watch?v=dKNPYX4yQbg&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=8&ab_channel=TrainITHard)
 - [Урок 9 - Групповые политики](https://www.youtube.com/watch?v=-hKi8qhDLkg&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=9&ab_channel=TrainITHard)
 - [Урок 15 - Файловый сервер](https://www.youtube.com/watch?v=F5K5m0BIbhg&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=18&ab_channel=TrainITHard)
 - [Урок 16 - FSRM](https://www.youtube.com/watch?v=AwUQYdvmnaw&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=19&ab_channel=TrainITHard)

##### Зарубежный материал
 - [How to create home folders](https://www.youtube.com/watch?v=Ry2--wJ9Tfc&ab_channel=ittaster)
