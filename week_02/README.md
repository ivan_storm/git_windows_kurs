## Содержание
- [Содержание](#содержание)
- [Введение](#введение)
- [Топология инфраструктуры Microsoft](#топология-инфраструктуры-microsoft)
- [Лабораторная работа №3. Основы Active Directory и управления объектами](#лабораторная-работа-3-основы-active-directory-и-управления-объектами)
- [Лабораторная работа №4. Общий доступ к файлам в Windows Server, FSRM, пространство DFS (общий доступ, разграничение прав, RAID-массив, DFS, квоты)](#лабораторная-работа-4-общий-доступ-к-файлам-в-windows-server-fsrm-пространство-dfs-общий-доступ-разграничение-прав-raid-массив-dfs-квоты)
- [Лабораторная работа №5. Групповые политики](#лабораторная-работа-5-групповые-политики)
- [Материалы для выполнения лабораторных работ 3-5](#материалы-для-выполнения-лабораторных-работ-3-5)
    - [Лабораторная 3](#лабораторная-3)
    - [Лабораторная 4](#лабораторная-4)
    - [Лабораторная 5](#лабораторная-5)
  - [Видеоматериалы](#видеоматериалы)
    - [Общий материал](#общий-материал)
    - [Основы Windows Server](#основы-windows-server)
      - [TrainIT Hard](#trainit-hard)
      - [Зарубежный материал](#зарубежный-материал)
- [Отчет по выполнению задания](#отчет-по-выполнению-задания)
    - [Лабораторная работа №3](#лабораторная-работа-3)
    - [Лабораторная работа №4](#лабораторная-работа-4)
    - [Лабораторная работа №5](#лабораторная-работа-5)

## Введение
Ниже представлена топология инфраструктуры, которая получится у вас по итогу прохождения курса. Лабораторные работы 3-5 представлены ниже. В самом конце присутствуют материалы, которые помогут выполнить данные лабораторные работы. Перед выполнением настоятельно рекомендуется изучить указанные статьи, а также видеоматериалы от TrainIT Hard.

Все лабораторные полностью прорешаны в полном объеме. В случае нахождения каких-либо багов или неточностей в описании просьба подсвечивать в нашем телеграм чате в обсуждениях.

Отчет присылать в формате Word файла на электронную почту.

__ВНИМАНИЕ! Для корректного выполнения лабораторных необходимо использовать клиентскую ОС Windows 10 LTSC, так как это корпоративная версия. В случае использования другой версии может не всё работать!__

Ссылки на образы с Яндекс диска:
- [Windows Server 2019](https://disk.yandex.ru/d/44SbQmvP3eS8HA)
- [Windows 10 1809 LTSC](https://disk.yandex.ru/d/Ug0FF9a5hC7aJA)

__ВНИМАНИЕ! Касаемо ресурсов. На ВМ можно выдавать меньше ресурсов, чем указано на схеме. На схеме рекомендумые параметры. Также необязательно держать все машины включенными одновременно (например, нет нужды держать включенной машину WIN-WD-LAB02 постоянно и т.д. Смотрите по тому, что необходимо на текущий момент.)__

---
## Топология инфраструктуры Microsoft
![Топология инфраструктуры](https://gitlab.com/ivan_storm/git_windows_kurs/raw/main/windows_scheme_3.0.png)

## Лабораторная работа №3. Основы Active Directory и управления объектами
 1. Установить службу каталогов Active Directory на сервер WIN-SRV-LAB01
    1. Имя домена - __grenlab.local__
    2. Пароль администратора домена __P@ssw0rd__, пароль для восстановления каталогов такой же
 2. Создать главную организационную единицу с именем домена grenlab, а в ней следующие организационные единицы в AD:
 	- MSK
 	- NN
 	- SPB
 	- EKB
 	- свой город (название для OU на свое усмотрение, например, мой город Заволжье (Zavolzhye) - ZVL)
  
 	В каждом организационном подразделении создать следующие:
 	- __Admins__ - OU для хранения объектов, связанных с администрированием
 	- __Computers__ - OU для хранения объектов типа компьютера
 	- __Disabled Users__ - OU для хранения отключенных объектов типа пользователи
 	- __Groups__ - OU для хранения объектов типа группы
 	- __Printers__ - OU для хранения объектов типа пользователи и группы для принтеров
 	- __Users__ - OU для хранения объектов типа пользователи

	Для выполнения данного задания рекомендуется написать скрипт на Powershell.
 3. Создать своего пользователя в __Active Directory__:
    1. Логин учетной записи (УЗ) должен быть в формате __<Первый символ имени>__ + __<Первый символ отчества>__ + __<Фамилия>__, например __SMIvanov__
    2. Отображаемое имя должно соответствовать __полным ФИО__
    3. Пароль должен быть __P@ssw0rd_23__
    4. В дополнительных атрибутах пользователя добавить свой номер мобильного телефона, город проживания и должность (например, студент)
    5. УЗ поместить в подразделение __Users__ своего города
    6. Добавить __свою УЗ__ в группу доменных администраторов
 4. Создать группы безопасности в каждой организационной единице в формате __\<OU>-SharedFolders-Users__, __\<OU>-PrintersAccess__ и __\<OU>-City-Users__
 5. Создать организационное подразделение Groups в grenlab, в Groups создать группы __CertAutoEnroll-Users__, __VIP-Users__, __Folders-Admins__, __Grenlab-WD-Admins__ и __NetworkDevice-MGMT-Admins__. В группу __NetworkDevice-MGMT-Admins__, __Folders-Admins__ и __Grenlab-WD-Admins__ добавить своего пользователя
 6. Создать OU __Servers__ в __grenlab__
 7. Создать учетные записи из __CSV-файла__:
    1. Каждый пользователь должен попасть в OU в соответствии со своим городом
    2. Пароль для всех пользователей использовать __P@ssw0rd__
    3. Каждый пользователь должен быть добавлен в группу __\<OU>-SharedFolders-Users__, __\<OU>-PrintersAccess__ и __\<OU>-City-Users__, где OU это OU города
    4. Данные должны заполниться согласно файлу. Необходимо применить PowerShell
    5. Пользователи с пометкой __VIP__ должны попасть в группу __VIP-Users__
 8. На клиентских компьютерах:
    1. удалить все лишние записи из файла __hosts__
    2. Ввести компьютер __WIN-WD-LAB01__ в домен
    3. Перенести объект в OU __MSK->Computers__
    4. Залогиниться под своим пользователем, потом под любым созданным пользователем
    5. Аналогичные действия сделать с хостом __WIN-WD-LAB02__
 9. Настроить парольную политику для группы __NetworkDevice-MGMT-Admins__ следующим образом:
    - Минимальное кол-во символов __10__
    - спецсимволы, разный регистр
    - смена пароля раз в __15 дней__
    - минимальное время для смены пароля __1 день__
    - число запоминаемых паролей 6
 10.  Выполнить настройку сайтов в AD
      1.   Создать дополнительный сайт для офиса Екб с именем __EkaterinburgSite__
      2.   Разнести все сети по сайтам
      3.   Изменить время репликации на минимально-допустимое
 11.  Включить корзину в __Active Directory__
     1.  В организационном подразделении grenlab создать три объекта типа компьютер с именами __TEMP-WD-01-3__ и удалить их
     2.  Восстановить объект __TEMP-WD-02__ в OU __grenlab__ в __Restored__
 12.  Создать и настроить виртуальную машину с Windows Server 2019:
      1.  IP-адрес из сети __10.0.10.0/24__, DNS - домен контроллер
      2.  Имя сервера __WIN-SRV-LAB03__
      3.  Ввести сервер в домен __grenlab.local__, переместить объект в ранее созданное организационное подразделение __Servers__
      4.  Добавить сервер в менеджер управления серверами на __WIN-SRV-LAB01__
 13.  Создать и настроить виртуальную машину с Windows Server 2019 (__ВНИМАНИЕ! Для тех, у кого мало ресурсов можно выключить двух клиентов и WIN-SRV-LAB03__):
      1.   IP-адрес из сети __10.0.40.0/24__, DNS - домен контроллер
      2.   Имя сервера __WIN-SRV-LAB04__
      3.   Добавить второй сетевой адаптер __vmbr41 (vlan 40)__, будет использоваться для связи с клиентом __WIN-WD-LAB03__
      4.   Установить роль __Remote Access__, настроить только __LAN Routing__. Firewall можно отключить. Настроить маршрутизацию, чтобы клиент __WIN-WD-LAB03__ мог получить доступ до ресурсов домена. Со стороны __WIN-SRV_LAB02__ также необходимо настроить маршрут до подсети __10.0.41.0__
      5.   Установить временную зону __Екатеринбурга (+5)__
      6.   Ввести сервер в домен __grenlab.local__
      7.   Установить роль __Active Directory__
      8.   Сделать данный сервер __Read Only Domain Controller__
		- Запретить репликацию паролей группы __NetworkDevice-MGMT-Admins__
		- Сайт AD - __Ekaterinburg__
		- Репликацию разрешить только с DC __WIN-SRV-LAB01__
 14. Создать клиентскую виртуальную машину __WIN-SRV-LAB03__
     1.  Имя машины __WIN-SRV-LAB03__
     2.  ip адрес статический __10.0.41.10/24__, шлюз - интерфейс __WIN-SRV-LAB04__, DNS - домен-контроллер, а также RODC как альтернатива
     3.  Ввести машину в домен __grenlab.local__
     4.  Перенести объект в OU __EKB__ -> __Computers__
 
 ## Лабораторная работа №4. Общий доступ к файлам в Windows Server, FSRM, пространство DFS (общий доступ, разграничение прав, RAID-массив, DFS, квоты)
 1. Установить на сервер __WIN-SRV-LAB01__ службы __FSRM__ и __DFS__
 2. Создать два сетевых диска на __WIN-SRV-LAB01__
    1. Добавить 4 жестких диска к виртуальной машине по __5 ГБ__
    2. Собрать каждые два диска в __RAID 1__ массивы (__RAID 1__ это зеркалирование, по итогу из двух дисков отобразится подключенным 1)
    3. Таблицу разделов использовать __GPT__, файловая система __NTFS__
    4. Логическим разделам задать метки __S__ и __K__
    5. Метки разделов - __Net Disk__
 3. Создать следующую структуру папок:
	- Home_Folders (Диск S)
	- Shared_Folders (Диск K)
	- Soft_Folders (Диск K)
	- City_Folders (Диск S), в нём названия папок в соответствии с именем __OU__
	- Documents_Folders (Диск C __WIN-SRV-LAB03__)
	- Photo_Folders (Диск C __WIN-SRV-LAB03__)
	- Ekb_Folders (Диск С __WIN-SRV-LAB04__)
 4. Выдать права на каталоги следующим образом:
    - Home_Folders -> __Domain Users__, разрешить создание папок внутри каталога, в самой папке владельцу можно делать что угодно, у остальных пользователей доступа быть не должно (кроме администраторов)
    - Shared_Folders -> __Domain Users__, только чтение, __Shared_Folders_Admins__ - полные права
    - Soft_Folders -> __Shared_Folders_Admins__, полные права, __Domain Users__ - чтение
    - City_Folders с названием OU (кроме Екб) -> __\<OU>-SharedFolders-Users__, права на чтение, полные права - у админов, у других прав на просмотр быть не должно
 5. Настроить общий доступ ко всем каталогам в соответствии с необходимыми правами
 6. Создать квоты на каталоги следующим образом:
	- Home_Folders - каждому пользователю по __250 Мб__
	- Shared_Folders - общая квота __2 Гб__
 7. Создать пространство __DFS__ 
    1. Создать пространство __DFS__, назвать __Grenlab_DFS__
    2. Связать  папки следующим образом:
       - Общие папки -> Shared_Folders
       - Программное обеспечение -> Soft_Folders
       - Документы -> Documents_Folders (__с WIN-SRV-LAB03__)
       - Фотографии -> Photo_Folders (__с WIN-SRV-LAB03__)
       - Москва -> каталог Москвы (диск S)
       - Нижний Новгород -> каталог НН (диск S)
       - Екатеринбург -> каталог ЕКБ (__с WIN-SRV-LAB04__)
       - Санкт-Петербург -> каталог СПБ (диск S)
       - Свой город -> каталог свой город (диск S)

 8. Реализовать __Home Folders__ для доменных пользователей
    1. В качестве сетевой шары использовать __Home_Folders__
    2. У каждого пользователя должна быть своя папка и доступ только к ней
    3. Домашний каталог необходимо прописать в соответствующем поле пользователя в AD, метка диска H

## Лабораторная работа №5. Групповые политики
1. Создать следующие групповые политики:
    1. Отключить __приветственную анимацию__ на всех клиентских ПК. Имя политики __DisableFirstSignAnimation__
    2. Включить на всех клиентских ПК возможность запуска от имени другого пользователя в меню __Пуск__. Имя политики __StartMenuProgramFromOtherUser__
    3. Блокировка компьютера при простое __3 минуты__, отключить __заставку__ и требовать __пароль__ при разблокировке. Имя политики __LockPCAfter5Minutes__
    4. Добавление пользователей группы __Grenlab-WD-Admins__ в локальную группу администраторов. Имя политики __LocalAdmins__
    5. В политики домена __по умолчанию__ включить параметр __Всегда ждать запуска сети при запуске и входе в систему__ (Если не включить, то софт в следующих пунктах не сможет установиться)
    6. Установить браузер __Mozilla Firefox__ через политики на все компьютеры и сделать его браузером по умолчанию (использовать файл ассоциаций для установки ПО по умолчанию). Имя политики __FirefoxInstall__
    7. Установить __Notepad++__ на все компьютеры. Имя политики __NotepadPPInstall__
    8. Распространить следующие ярлыки на рабочий стол:
		- Telegram Web (ссылка на веб-версию телеграм, использовать оригинальную иконку)
		- Общие папки (каталог __Shared_Folders__)
		- __\<City>__ (каталог из __City_Folders__, использовать нацеливание на группу)
  	9. Разрешить в __Windows Firewall__ (для доменного профиля) полностью __ICMP__, __SSH__, запретить доступ до следующих сайтов по __http/https__ (имя политики __FirewallConfiguration__):
		- it-learn.space (также запретить 22 порт)
		- vk.com
		- mail.ru
	10.  Настроить часовой пояс Екатеринбург для офиса в Екатеринбурге (__применение политик по сайтам__) (учитывать службу W32Time на клиентских машинах)
	11. Установить фоновую картинку на рабочий стол для группы __VIP-Users__ через GPO - https://www.goodfon.ru/hi-tech/wallpaper-download-1920x1080-windows-xp-sinii-zelionyi-oblaka-oboi-klassika.html
	12. Подключение сетевого диска __DFS__ всем доменным пользователям с меткой __D__
	13. На OU __Servers__ применить политику по смене фона рабочего стола. Картинку использовать на свое усмотрение
2. Проверить результаты применения групповых политик на клиентских ПК
---
## Материалы для выполнения лабораторных работ 3-5
#### Лабораторная 3
- [Установка и настройка Windows Server на примере версии 2022](https://pyatilistnik.org/installing-and-configuring-windows-server-2021/)
- [Active Directory от А до Я](https://pyatilistnik.org/vvedenie-v-osnovnyie-ponyatiya-active-directory/)
- [Обзор доменных служб Active Directory. Документация Microsoft](https://learn.microsoft.com/ru-ru/windows-server/identity/ad-ds/get-started/virtual-dc/active-directory-domain-services-overview)
- [Установка Active Directory](https://itproblog.ru/%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-%D0%B8-%D0%BF%D0%B5%D1%80%D0%B2%D0%BE%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA/)
- [Список инструментов по управлению Active Directory](https://pyatilistnik.org/active-directory-management-administrative-tools/)
- [Создание и управление сайтами AD](https://vmblog.ru/sajty-podseti-active-directory/)
- [Создание пользователей в Active Directory](https://winitpro.ru/index.php/2018/03/02/new-aduser-sozdaem-polzovatelej-v-domene/)
- [Редактор атрибутов Active Directory](https://winitpro.ru/index.php/2019/08/15/active-directory-attribute-editor/)
- [Делегирование административных полномочий в Active Directory](https://winitpro.ru/index.php/2018/12/29/active-directory-delegirovanie-admin-prav/)
- [Используем Powershell для работы с AD](https://winitpro.ru/index.php/2019/07/18/modul-active-directory-dlya-powershell/)
- [Импорт данных из CSV файлов в PowerShell](https://winitpro.ru/index.php/2022/10/13/import-csv-fajlov-v-powershell/)
- [Как добавить компьютер в домен Active Directory](https://winitpro.ru/index.php/2022/10/06/dobavit-kompyuter-windows-v-domen-ad/)
- [Корзина Active Directory](https://habr.com/ru/companies/veeam/articles/324246/)
- [Контроллер домен Read-Only (RODC)](https://winitpro.ru/index.php/2010/11/21/rabota-s-kontrollerami-domena-read-only-rodc-chast-1/)
- [Установка и настройка RODC](https://winitpro.ru/index.php/2017/11/08/ustanovka-rodc-kontrollera-domena-na-windows-server-2016/)
---
#### Лабораторная 4
- [Настройка программного RAID](https://internet-lab.ru/software_raid_windows_server_2016)
- [Общий доступ к файлам и папкам](https://coderlessons.com/tutorials/microsoft-technologies/izuchite-windows-server-12/windows-server-2012-obshchii-dostup-k-failam)
- [Диспетчер ресурсов файлового сервера FSRM](https://wiki.merionet.ru/articles/ustanovka-i-nastrojka-dispetchera-resursov-fajlovogo-servera-file-server-resource-manager-v-windows-server-2016)
- [Динамический контроль доступа: списки свойств ресурсов и классификация файлов](https://habr.com/ru/articles/204212/)
- [Home Folders in Active Directory](https://sanuja.com/blog/how-to-create-home-folders-in-active-directory)
- [Distributed File System. Архитектура и базовые понятия](https://windowsnotes.ru/activedirectory/distributed-file-system-arxitektura-i-bazovye-ponyatiya/)
- [Установка и настройка DFS и репликации файлов](https://winitpro.ru/index.php/2013/09/25/ustanovka-i-nastrojka-dfs-i-replikacii-fajlov-v-windows-server-2012/)

#### Лабораторная 5
- [Что такое групповые политики (GPO)](https://habr.com/ru/companies/galssoftware/articles/543588/)
- [Управление групповыми политиками](https://1cloud.ru/help/windows/gruppovye-politiki-active-directory)
- [Установка программ через GPO](https://winitpro.ru/index.php/2011/10/21/ustanovka-programm-s-pomoshhyu-gruppovyx-politik/)
- [Почему не применяются GPO](https://winitpro.ru/index.php/2019/03/18/primenenie-gpo-spravka-admina/)
- [Создание ярлыков через GPO](https://winitpro.ru/index.php/2019/12/04/sozdat-yarlyk-na-rabochem-stole-gpo/)
- [Подключение сетевых дисков через GPO](https://winitpro.ru/index.php/2020/07/14/podklychit-setevoy-disk-windows-gpo/)
- [Настройка фаерволла через GPO](https://winitpro.ru/index.php/2018/12/06/nastrojka-pravil-windows-firewall-gpo/)
---
### Видеоматериалы
#### Общий материал
- [Основы Powershell](https://info-comp.ru/sisadminst/546-windows-powershell-basics.html)
- [Работы с массивами в Powershell](https://fixmypc.ru/post/rabota-v-powershell-s-massivami-i-listami-na-primerakh/)

#### Основы Windows Server
##### TrainIT Hard
 - [Урок 5 - Основы Active Directory Domain Services](https://www.youtube.com/watch?v=aytPzZ0djmQ&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=6&ab_channel=TrainITHard)
 - [Урок 6 - Advanced DNS, интеграция AD DS и DNS](https://www.youtube.com/watch?v=eKq7xROGpYo&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=6&ab_channel=TrainITHard)
 - [Урок 7 - Сайты и репликация Active Directory](https://www.youtube.com/watch?v=45FzcvixuZs&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=7&ab_channel=TrainITHard)
 - [Урок 8 - Доверие и группы безопасности в AD](https://www.youtube.com/watch?v=dKNPYX4yQbg&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=8&ab_channel=TrainITHard)
 - [Урок 9 - Групповые политики](https://www.youtube.com/watch?v=-hKi8qhDLkg&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=9&ab_channel=TrainITHard)
 - [Урок 15 - Файловый сервер](https://www.youtube.com/watch?v=F5K5m0BIbhg&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=18&ab_channel=TrainITHard)
 - [Урок 16 - FSRM](https://www.youtube.com/watch?v=AwUQYdvmnaw&list=PLUNgQQczUJbveKhzohlY4uNGZYkZm_VDZ&index=19&ab_channel=TrainITHard)

##### Зарубежный материал
 - [How to create home folders](https://www.youtube.com/watch?v=Ry2--wJ9Tfc&ab_channel=ittaster)

## Отчет по выполнению задания

#### Лабораторная работа №3
Результаты выполнения команд в виде скриншотов.
__Все действия выполняем на сервере с домен-контроллером WIN-SRV-LAB01 (за исключением, где четко написано на каком сервере выполнять)!__
```powershell
Get-DnsServerZone
Get-ADDomain
Get-ADDomainController
(Get-ADOrganizationalUnit -Filter * -SearchBase "DC=grenlab,DC=local").DistinguishedName
(допонительно скриншот иерархии подразделений в оснастке Active Directory Users and Computers)
Get-ADUser <username> -Properties Title, City, MobilePhone, mobile, DistinguishedName
(Get-ADGroup -Filter * -SearchBase "OU=grenlab,DC=grenlab,DC=local").Name
Get-ADComputer -Filter { Name -like "WIN-WD*" } | select DistinguishedName
Get-ADReplicationSiteLink -Filter *
Get-ADDomainController -Filter * | select HostName, IsReadOnly, Fores, Enabled, Site, OperationMasterRoles
repadmin /replsum
repadmin /showism
repadmin /showrepl
repadmin /queue
repadmin /showbackup *
repadmin /syncall /AeD
repadmin /replsum
w32tm /monitor
Get-ADOptionalFeature "Recycle Bin Feature" | select Name, EnabledScopes
Get-ADComputer TEMP-WD-02 -Properties * | select isDeleted
Get-ADObject -Filter { Name -like "*TEMP*" } -IncludeDeletedObjects
Get-ADDefaultDomainPasswordPolicy
```
Добавить любого пользователя в группу NetworkDevice-MGMT-Admins, поменять ему пароль на P@ssw0rd_1327 после чего выполнить команду на сервере AD:

```powershell
Get-ADUser <имя пользователя> -Properties msDS-UserPasswordExpiryTimeComputed | Select-Object name,samaccountname,@{Name="Expired";Expression={[datetime]::FromFileTime($_."msDS-UserPasswordExpiryTimeComputed") }}
```

Результат команды можно скопировать в текстовый файл и вложить в отчет. Также в отчет необходимо положить скрипт
```powershell
Get-ADUser -Filter * -SearchBase "OU=grenlab,DC=grenlab,DC=local" -Properties * | select userprincipalname, givenname, surname, displayname, company, department, title, mobilephone, mobile, created,lastlogondate,city,distinguishedname
```

#### Лабораторная работа №4
Результаты выполнения команд в виде скриншотов.

1) На WIN-SRV-LAB01 выполнить: 
	```powershell
   Get-WindowsFeature -Name FS-Resource-Manager, FS-FileServer, RSAT-FSRM-Mgmt, FS-DFS-Namespace
   ```
2) С сервера __WIN-SRV-LAB01__ скриншот __diskmgmt.msc__, обязательно должны быть видны добавленные диски
3) На __WIN-SRV-LAB01__ выполнить `tree K:\` и `tree S:\`, на __WIN-SRV-LAB03__ `tree C:\` и на __WIN-SRV-LAB04__ `tree C:\`
4) На __WIN-SRV-LAB01__, LAB03 и LAB04 выполнить `net share`
5) На сервере __WIN-SRV-LAB01__ сделать скриншот раздела __Quotas__ в FSRM
6) На сервере __WIN-SRV-LAB01__ выполнить (результаты можно закинуть в txt файл и вложить в отчет):
	```powershell
   Get-Acl S:\Home_Folders | fl
	Get-Acl S:\Home_Folders\<папка вашего пользователя>| fl
	(Get-ChildItem S:\ -Recurse -Directory | Get-Acl) | fl
   ```
7) На __WIN-SRV-LAB01__ выполнить команду `Get-DfsnRoot`, также сделать скриншот namespaces __grenlab.local\grenlab_dfs__
8) Выполнить `Get-ADUser -Filter * -SearchBase "OU=grenlab,DC=grenlab,DC=local" -Properties * | select UserPrincipalName, HomeDirectory, HomeDrive `

#### Лабораторная работа №5
Результаты выполнения команд в виде html файлов и скриншотов.
На каждой клиентской машине необходимо выполнить `gpupdate /force`
1) На каждой клиентской машине зайти под доменным пользователем и выполнить `gpresult /h user_report.html`. Полученный html файл вложить в отчет. Также сделать `gpresult /r` и приложить текстовым файлом
2) На каждой клиентской машине запустить от имени администратора и выполнить `gpresult /h full_report.html` (в нем должны быть отображены политики на компьютер). Полученный html файл вложить в отчет. Также сделать `gpresult /r` и приложить текстовым файлом.
3) Скриншот окна редактора групповых политик, где виден список всех политик
4) Скриншоты рабочего стола каждого клиентского компьютера, а также с серверов
5) С машины __WIN-WD-LAB03__ результат выполнения команды `Get-Timezone`
6) Скриншот рабочего стола с любого клиентского ПК под пользователем из группы __VIP-USERS__. Также выполнить `gpresult /h vip_report.html` и файл вложить в отчет
7) Скриншот подключенного DFS диска с любого клиентского ПК под любым пользователем
8) Скриншот с выполнением команд telnet mail.ru 443 и ping mail.ru/vk.com